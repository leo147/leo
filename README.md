# Longwood Environmental Observatory (LEO)
## Data and Analysis Code

The Longwood Environmental Observatory – is a multidisciplinary environmental observatory operated by Longwood University that provides hands-on, inquiry-based learning and research experiences for students. Our goals are to provide students experience gathering and analyzing ecological data and make public research-grade data for scientists, educators, and the community.

As environmental stewards, our mission is to facilitate hands-on, inquiry-based learning and produce high quality data that fosters knowledge, discussion, and collaboration among researchers, educators, students, and the public.

More information can be found at [leo.longwood.edu](leo.longwood.edu).

This repository contains all of the lab notes, field note, code and data used by the LEO team.

This work is licensed under a Creative Commons Attribution 4.0 International License.
