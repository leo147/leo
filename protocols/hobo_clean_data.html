<html>
	<head>
<style>

body {
	padding: 5em;
	font-family: sans-serif;
}
 
li {
    padding: 0.3em;
   }

</style>
</head>
<body>

<h1>Protocol for Cleaning Data from HOBO sensors after Downloading</h1>
<h2>Longwood Environmental Observatory</h2>

<h3>Document Data</h3>
		
* File created: 2021-10-05 - Kenneth Fortino
* Modified: 2022-09-01 - KF - updated protocol to reflect new file name protocols

<h3>Description</h3>
<p>
This file describes the procedure for cleaning and preparing data that has been downloaded from the HOBO DO, CT, and pressure sensors for uploading onto Zenodo.
</p>
<p>
Before using this protocol, the data should have already been downloaded from a HOBO DO, CT, or pressure sensor and be stored on a LEO laptop (LEO1 or LEO2) as a _raw.csv file. See the <code>hobo_download_launch.html</code> for how to download the data from the HOBO sensors.
</p>

<h2>Pull from Gitlab to Update the Files</h2>
<p>
Before changing anything on the local machine (i.e., the computer you are using), you need to update all the files from the cloud (Gitlab).
</p>
<p>
To do this:
<ol>
  <li>Click on the "red-green-yellow-blue diamond" in the task bar. This will open a terminal window.</li>
  <li>Type <code>cd</code>. This will bring you to the "home" directory.and you should see a ~ at the end of the line above the "$" prompt</li>
  <li>Type <code>cd LEO/LEO_Analysis</code>. The directory name should now be "~/LEO/LEO_Analysis (master)".
    <ul>
      <li>NOTE: in the terminal you can use TAB to auto-complete commands</li>
    </ul>
  </li>
  <li>To pull the changes from the cloud: type <code>git pull origin master</code>. This will do variable things depending on whether there are any changes to pull. When it is done your local machine will be up-to-date with the Gitlab.</li>
</ol>


<h2>Clean Data from Sensors</h2>
<p>
The data from the weather station should be stored on the laptop in the <code>data</code> directory in the <code>LEO_Analysis</code> repository. (Path: <code>LEO_Analysis/lab_notebook/data</code>)
</p>
		
<h3>Navigate to the correct working directory</h3>
<p>
To begin:
<ol>
  <li>Open R Studio</li>
  <li>Select: Session &rarr; Set Working Directory &rarr; Choose Directory</li>
  <li>When the window opens, select This PC &rarr; Windows (C:) &rarr; Users &rarr; user &rarr; LEO &rarr; LEO_Analysis.</li>
</ol>
</p>
<p>At this point if you click on the "Files" tab in the lower right corner of R Studio you should see "LEO_Analysis".</p>
		
<h3>Open the cleaning script</h3>
<p>
The cleaning script is the R code that creates a final LEO data file from the raw downloaded data.
</p>
<p>
To begin:
<ol>
  <li>Click on LEO_Analysis, then lab_notebook, then analysis in the "Files" tab. You should now see a list of files ending in .R</li>
  <li>Click on "SITE_weather_datafile_cleaning.R", where SITE is either "HS" or "BC" (for "Hull Springs" or "Buffalo Creek", which is Farmville). The script should open in the upper left panel of R Studio.</li>
  <li>Confirm that the script is the correct one for the site that you are cleaning.</li>
</ol>
</p>

<h3>Run the script</h3>
<p>
A script is a computer program that is run line-by-line rather than all at once, so to run the script you will enter the required information and then run each line. Most of the script will not be modified but you will need to update some information specific to each download each time you run the script.
</p>
<p>
This cleaning script works in 3 main steps:
<ol>
  <li>Create objects (specific variables in R) that contain the raw data or information that will be used to make new data files.</li>
  <li>Manipulate the data in the objects to make it more compatible with data analysis and statistics. 
    <ul>
      <li>Note that the raw data files are <em>never</em> changed. Only the contents of the objects are changed. This means that the cleaning can always be undone.</li>
      <li>The main changes that are made are to remove metadata into separate objects, combine the data from tables 1 and 2 into a single file, and format dates and times to a more universal format.</li>
    </ul>
  <li>Write the contents of the objects to new files that will be used for all future data analysis and statistics. All work with the data will be done with these files. The original raw data file is never altered.</li>
</ol>
</p>

<h4>Enter the information specific to this download</h4>

<p>
All of the data files downloaded from the HOBO sensors should be in the format: <code>SITE_SENSOR_DATE-RANGE.csv</code>, where SITE is either HS for Hull Springs or BC for Buffalo Creek, SENSOR is either DO for the dissolved oxygen sensor, CT, for the conductivity sensor, PRESS for the pressure transducer in the water (i.e., the water level logger), and BP is for the pressure transducer in the air (i.e., barometric pressure). DATE-RANGE is the begin date to the end date in YYYY-MM-DD_YYYY-MM-DD format.
</p>
<p>
In the script the only fields that you should change are in the section labeled: ## Enter User Data, ### Input File Names
</p>
<p>
This section of the script assigns R objects to the contents of the data files or specifies the file name to write an object's information to.
</p>
<p>
In general, the only thing that you will need to change is the date portion of the file names but you should confirm that the rest of the file name matches the format above or specified in the script
</p>
<p>
To begin:
<ol>
  <li>Change the date on each file name so that it matches the date on the downloaded file to be cleaned.</li>
  <li>Save the updated version of the script by clicking the "save" icon below the file name in the panel.</li>
</ol>
</p>

<h4>Execute the script commands</h4>
		
<p>
Once you have changed the variable data as described above, you can not execute (run) the code in the script.
</p>
<p>
To run a line of code in the script in R Studio, place the cursor anywhere on that line and press <code>CTL-ENTER</code>.  Alternatively, you can press the "Run" button in the upper-right of the panel with the script.
</p>

<p>
<strong>NOTE:</strong> If the line of code does not run or you don't see the "Run" button, check to make sure that the field in the lower-right of the script panel says "R Script". If it does not, change it to "R Script" with the drop-down-menu.
</p>
<p>
Run the lines of code one at a time and look for error messages in the console panel in the lower-left of R Studio. Running a line of code may not always appear to have done anything but if there is no error message, then proceed. If you get an error message, the easiest thing to do is start over from the top. Since you never alter the raw data files, you can run the script over and over again and it will just re-write the objects and files.
</p>
<p>
The lines preceded by any number of <code>#</code> characters are comments to help you navigate and understand what the code is doing. The number of <code>#</code> characters indicated header levels so <code>#</code> is a main header, <code>##</code> is a sub-header, etc...
</p>

		
<h2>Confirm that the clean files were made</h2>

<p>
When you have completed running all of the lines of the code the script should have created two new files in <code>LEO_Analysis\lab_notebook\data</code>. Check to make sure there is a file with the file name that you entered for <code>output.clean.file</code> and <code>metadata.file</code>
</p>
<p>
Once you have confirmed that these files exist, you can open them in R Studio by clicking on the file name and selecting "View File". Open each file and confirm that they contain the appropriate data. You won't be able to really read the data but there should be a lot of it and it should look like numbers in rows or blocks of readable text.
</p>
<p>
If everything looks good, check to make sure that the script changes were saved (the file name will be black if all changes are saved and red if there are un-saved changes), and close R Studio. 
  <ul>
    <li>NOTE: R Studio will ask if you want to save the workspace. Select "Don't Save"</li>
  </ul>
</p>

<h2>Commit and Push the changes to Gitlab</h2>
		
<p>
Whenever you change or create files on a local machine (i.e., the LEO1 or LEO2 laptops), you need to commit those changes to git and then push them to the cloud storage repository in Gitlab.
</p>
<p>
You can do this as often as you would like. Think of this as "saving" your work, so if you have stop working and come back to it, you should commit and push the changes
</p>

<h3>Commit changes</h3>
<ol>
  <li>In the terminal (the red-green-yellow-blue diamond), type <code>cd ~/LEO/LEO_Analysis </code> to get back to the main repository directory. The line above the prompt should say <code>cd ~/LEO/LEO_Analysis (master) </code>.</li>
  <li>Type <code>git add *</code>. This tells git that you want to commit all the changes that you made to the repository.</li>
  <li>Type <code>git commit -am "COMMIT MESSAGE"</code>. The COMMIT MESSAGE should be a one sentence description of what changes you made (i.e., "Cleaned the HS weather data from 2021-05-04"). These notes help us find the "chunks" of information stored in the repository, so make sure they are brief but informative.</li>
  <li>Type <code>git push origin master</code>. This will push the changes to Gitlab. In other words, update the repository in the cloud to match the one on your local computer. "Origin" is the cloud repo, "Master" is the local repo.</li>
</ol>

<h2>Document your work</h2>
<p>
Make sure you update your Work Journal with what you did and move/update any relevant Trello cards.
</p>

	</body>
</html>
		

		<!--
		
		<h5>Create data objects</h5>
		<p>These lines of code store the data into objects in R</p>
		<ol>
			<li>Run all the lines of code in the <code>## Enter User Data</code> Section. This will create a series of objects in the upper-right panel of R Studio that contain information needed by the rest of the script.</li>
			<li>Run the two lines of code in <code>## Import the data file as text</code>. This copies the data from the raw data files into two objects that can now be manipulated without altering the raw data file.</li>
		</ol>

		<h5>Extract the metadata</h5>
		<p>The file that comes off of the data logger contains information that is important but will not be used for data analysis and statistics. These data are referred to as metadata. The next lines of code remove the metadata from the data and store it in a separate file.</p>
		<p>The code also properly formats the header information (i.e., column names) into the data file.</p>
		<ol>
			<li>Run all the lines of code in the <code>## Extract metadata</code> section.</li>
		</ol>
		<p>At this point all of the extracted metadata is stored inside R as objects but to use it outside R, it needs to be saved to a text file.</p>
		<ol>
			<li>Run the lines of code 

		-->
