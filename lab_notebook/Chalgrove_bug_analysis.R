# Analysis of the bug data from Chalgrove Lake

## Created 2022-04-12

## Load packages 

    library("tidyverse")

## Import Data
    
    bug <- read.csv("./data/BUG PROJECT DATA.csv", header = T)

    
## Summary Statistics
    
    bug %>%
      summarize(
        mean.abundance = mean(Abundance, na.rm = T),
        sd.abundance = sd(Abundance, na.rm = T),
        mean.richness = mean(Richness, na.rm = T),
        sd.richness = sd(Richness, na.rm = T)
      )
    
    ggplot(bug, mapping = aes(
      y = Abundance,
      x = Shore
    )
    )+
      geom_jitter(
        width = 0.1,
        size = 2
      )
    
    ggplot(bug, mapping = aes(
      y = Abundance,
      x = Location
    )
    )+
      geom_jitter(
        width = 0.1,
        size = 2
      )
    
    ggplot(bug, mapping = aes(
      y = Richness,
      x = Shore
    )
    )+
      geom_jitter(
        width = 0.1,
        size = 2
        )
    
    ggplot(bug, mapping = aes(
      y = Richness,
      x = Location
    )
    )+
      geom_jitter(
        width = 0.1,
        size = 2
        )
    
    ggplot(bug, mapping = aes(
      y = Abundance
    )
    )+
      geom_dotplot()
    