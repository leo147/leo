# Combine Weather Data from LEO	

## Load Packages

     library("tidyverse")

## Import Data
### Lancer Park Data
     
    LP1 <- read.csv("data/LP_weather_2021-11-19_2022-01-06.csv", header = T)
    LP2 <- read.csv("data/LP_weather_2022-01-06_2022-02-14.csv", header = T)
    LP3 <- read.csv("data/LP_weather_2022-02-14_2022-03-29.csv", header = T)
    LP4 <- read.csv("data/LP_weather_2022-03-29_2022-05-17.csv", header = T)
    LP5 <- read.csv("data/LP_weather_2022-06-21_2022-09-06.csv", header = T)
  
### Baliles Center Data
    
    BC1 <- read.csv("data/HS_weather_2021-10-28_2021-12-04_date_corr.csv", header = T)
    BC2 <- read.csv("data/HS_weather_2021-12-04_2022-02-05.csv", header = T)
    BC3 <- read.csv("data/HS_weather_2022-02-05_2022-03-08.csv", header = T)
    BC4 <- read.csv("data/BC_weather_2022-03-29_2022-05-17.csv", header = T)
    BC5 <- read.csv("data/BC_weather_2022-05-17_2022-06-21.csv", header = T)
    BC6 <- read.csv("data/BC_weather_2022-06-21_2022-09-06.csv", header = T)
    
## Join into a single data.frame
### Lancer Park Files
    
    LP <- full_join(LP1, LP2)
    LP <- full_join(LP, LP3)
    LP <- full_join(LP, LP4)
    LP <- full_join(LP, LP5)
  
### Baliles Center Files
   
    BC <- full_join(BC1, BC2)
    BC <- full_join(BC, BC3)
    BC <- full_join(BC, BC4)
    BC <- full_join(BC, BC5)
    BC <- full_join(BC, BC6)
    BC <- full_join(BC, BC3)
    
## convert TIMESTAMP and DATE to POSIX 
    
    LP$TIMESTAMP <- as.POSIXct(LP$TIMESTAMP)
    LP$DATE <- as.POSIXct(LP$DATE)
    
    BC$TIMESTAMP <- as.POSIXct(BC$TIMESTAMP)
    BC$DATE <- as.POSIXct(BC$DATE)

## Write to file
    
    write.csv(
    	LP,
    	file = "./data/LP_weather_2021-11-19_2022-09-06.csv",
    	row.names = F,
    	quote = F
    )
    	
    write.csv(
    	BC,
    	file = "./data/BC_weather_2021-10-28_2022-09-06.csv",
    	row.names = F,
    	quote = F
    )
    
# Test Plots
    
    plot(
    	Rain_mm_Tot ~ TIMESTAMP,
    	data = BC,
    	type = "l"
    )
    
    plot(
    	AirTC ~ TIMESTAMP,
    	data = BC,
    	type = "l"
    )
    
    plot(
    	SlrMJ_Tot ~ TIMESTAMP,
    	data = LP  ,
    	type = "l",
    	ylim = c(0, 1)
    )

# Test Analyses
    
    num_obs_freezing <- length(
    	w1$AirTC[
    		w1$AirTC > 0
    	]
    )