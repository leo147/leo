# Set-up Notes for LEO Dock Sampling Site
## Hull Springs
## 2021-04-30

## People
* Ken Fortino

## Description

These notes are for the re-set up of the Dock site at HS. All of the sensors were removed from the site in the winter of 2019 and have not been back in place.

The site will be set up to measure:

* Water temperature
* DO
* Cond
* pH
* fDOM
* Chl
* Water depth

## Equipment installed

* YSI EXO 2 Multiparameter Sonde
  * S/N: 15E100791
  * LU-tag:
  * Sensors Installed
    * ODO
    * Temp/Conductivity
    * Chl
    * Turbidity
    * fDOM
    * pH

* HOBO Water Level Logger - Titanium
  * P/N: U20-001-01 TI
  * S/N: 20844110
  * LU-tag: 08257
  * Name: HS-Dock

## Set-up Notes

### Water Level

I set all the logger to begin logging at 11:59 PM on Tuesday 2021-05-04. 
I set the logging interval to 10 minutes which should give 150 days of data.

## Installation Notes

* The YSI and pressure transducer were installed at the dock between 12:10 and 12:17 PM on 2021-05-04.
* The YSI was flashing red approx every 15 sec.
* Aimes Creek was full of detritus from a submerged macrophyte that was growing in the shallow headwaters. I removed the mats that were wrapped around the dock pilings where the mount went but I suspect they will come back when the wind shifts. 

