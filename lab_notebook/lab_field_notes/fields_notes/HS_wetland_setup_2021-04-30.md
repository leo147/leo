# Set-up Notes for LEO Wetland Sampling Site
## Hull Springs
## 2021-04-30

## People
* Ken Fortino

## Description

These notes are for the set up of the LEO sampling site at the Hull Springs Wetlands Site.

The site will be set up to measure:

* Water temperature
* Water conductivity
* Water depth

## Equipment installed

* HOBO Dissolved Oxygen Logger
  * P/N: U26-001
  * S/N: 20781166
  * LU-tag: 08125
  * Name: HS-Wetland_DO

* HOBO Water Level Logger - Titanium
  * P/N: U20-001-01 TI
  * S/N: 20844112
  * LU-tag: 08256
  * Name: HS-Wetland

* HOBO Conductivity Logger 
  * P/N: 
  * S/N: 
  * LU-tag: 
  * Name: HS-Wetland_CT

## Set-up Notes

I set all the loggers to begin logging at 11:59 PM on Tuesday 2021-05-04. 

### Conductivity Logger

The conductivity logger was not installed because the water was too shallow on the 2021-05-04 deployment.

The conductivity logger was added to the sensor array on the 2021-06-11 deployment because the sensors were mounted horizontally.

### Dissolved Oxygen

I installed a new sensor cap. The sensor cap will expire on 2021-11-29. I did a one-point 100% saturation calibration. I set the logging interval to 10 minutes, which will give 150 days of data.

## Water Level

I set the logging interval to 10 minutes which should give 150 days of data.

## Installation Notes

* The sensors were installed at approximately 1:14 PM on 2021-05-04
* The water level in the wetland at the installation site was approximately 12 cm.


