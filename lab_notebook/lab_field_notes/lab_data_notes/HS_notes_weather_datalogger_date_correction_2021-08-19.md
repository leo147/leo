# Data Adjustment Notes for LEO Wetland Sampling Site
## Hull Springs
## 2021-08-19

## People
* Ken Fortino

## Description

These notes are for the correction of the HS weather station data from the 2021-08-14 where the data logger reset its date value on "2021-08-05 23:00:00" to "1990-01-01 00:00:00". The data logger continued to collect data with this as the beginning date.

To correct the data I developed a R script that:

1. Calculated the difference between the actual time that the data logger was accessed ("2021-08-14 14:00:00") and the last data record from the data logger ("1990-01-09 06:30:00"). 

2. Converted the day result of the difference to seconds. The default resolution for the subtraction of POSIXct dates is "day", however to add to a POSIXct date, you need to add seconds. Therefore, before the time can be added to the incorrect values it needs to be converted to seconds. There was a difference of 11540.27 days (997079400 seconds) between the final incorrect TIMESTAMP and the time that the logger was accessed.

3. Applied the time difference to "correct" the erroneous dates. There was a 9-hour gap between the last correct TIMESTAMP ("2021-08-05 23:00:00") and the first incorrect TIMESTAMP ("1990-01-01 00:30:00") with the second correction added ("2021-08-06 08:00:00").

4. Replaced the incorrect dates with the corrected dates and then wrote the .csv file with the corrected dates.

The script can be found on GitLab:

[https://gitlab.com/leo147/leo/-/blob/master/lab_notebook/data_processing/cleaning_scripts/HS_weather_date_correction.R](https://gitlab.com/leo147/leo/-/blob/master/lab_notebook/data_processing/cleaning_scripts/HS_weather_date_correction.R)
