# Notes on data cleaning for BC weather

## Data collection range:
* 2022-09-06 to 2023-02-14

## Data files:
* `./data/raw_data/BC_weather_CR300_Table*_2022-09-06_2023-02-14.dat`

## Issues

1. The wind sensor is still reporting values that are too high to be reasonable. The mean recorded value is 14.78 m/s with a range from 0.00 to 130.7 m/s. The Campbell tech support thinks that the sensor may be malfunctioning but I am not sure this is the case since the issue began when the new program was uploaded.

2. The Rainfall sensor is not reporting any data after mid-November.

3. The Light (SlrW) sensor stops recording `0` at night around the end of December.

4. The date range is too great to be only the data from the previous download. The data begin on `2022-06-21` rather than the date of the last download which was `2022-09-06`. I assume that this is because I used the LEO 2 laptop for the last download and the LEO 1 laptop for this download, so the datalogger software did not know that the had been a September download.

