# Lab notes for chalgrove lake leaf packs 2022-02-18

## Created by Helena Loucas

## Description
Lab notes for the creation of leaf packs to catch aquatic insects in Chalgrove Lake

##Lab Procedure 
1. Used senescent maple leaves collected 2019
2. Weighed out 3 grams of leaves per pack
3. Filled bucket with warm water
4. Soaked leaves 3 grams at a time
5. Fan out leaves and clip at petiole
6. Insert soaked leaves into pack and clip pack at top
7. Attach clip to float
8. Store in bucket of water

##Field Procedure
1. Leaf packs were evenly placed along the North and South shore
2. 3 leaf packs were placed on each shore

