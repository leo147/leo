Water Pressure Metadata
﻿"Plot Title: LP_CampusPond_PRESS_raw"
"#","Date Time, GMT-04:00","Abs Pres, kPa (LGR S/N: 20810903, SEN S/N: 20810903)","Temp, °C (LGR S/N: 20810903, SEN S/N: 20810903)"
 Water Pressure Deploy Begin:
2022-05-11 14:35:00
Water Deploy End:
2022-06-13 08:52:00
Reference Depth Time
2022-06-13 08:55:00
Reference Depth (m)
0.85 m
BP Metadata
﻿"Plot Title: LP_CampusPond_BP_raw "
"#","Date Time, GMT-04:00","Abs Pres, kPa (LGR S/N: 20844113, SEN S/N: 20844113)","Temp, °C (LGR S/N: 20844113, SEN S/N: 20844113)"
BP Deploy Begin:
2022-05-11 14:35:00
BP Deploy End:
2022-06-13 08:52:00
Reference Depth Time
2022-06-13 08:55:00
Notes
none
