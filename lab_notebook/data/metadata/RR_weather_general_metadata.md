# General Metadata for Atmospheric Sampling Station at Randolph-Henry High School

## Files

Specific metadata for each deployment can be found as text files with the file format of:

    RR_weather_YYYY-MM-DD_YYYY-MM-DD_metadata.txt
    
Where YYYY-MM-DD_YYYY-MM-DD is the date range of the sampling period.

## Metadata File Created

  * 2022-05-10 by KF - copied and modified LP_weather_general_metadata.md
  
## File Modified

## Description

These data are from a weather station installed at Randolph-Henry High School (37.053282 N, -78.649005 W) as part of the Longwood Environmental Observatory (leo.longwood.edu).

All data are CC-BY and should be cited using the DOI available at https://zenodo.org/communities/leo/

## Station Specifics

  The specific at each site are:

    * Barometric Pressure (mmHg) - Campbell Scientific CS100 Barometric Pressure Sensor
    * Light Flux Density (kW/m^2) - Apogee Thermopile Pyranometer
    * Light Total Flux (kJ/m^2) - Apogee Thermopile Pyranometer
    * Rainfall (mm) - not installed
    * Temperature (dC) - HygroVUE5 Temperature and Relative Humidity Sensor
    * Relative Humidity (%) - Campbell Scientific CS215 Temperature and Relative Humidity Sensor
    * Wind Speed (m/s) - Wind Setry 03002-5 Wind Speed and Direction Sensor
    * Wind Direction (degrees from true N) - Wind Setry 03002-5 Wind Speed and Direction Sensor
    * Data Collection - Campbell Scientific CR800 Data Logger
    
The sensors are sampled every 15 minutes
 
## Measurement Parameters, units, and Variable Names

    * DATE - the date that the record was collected (YYYY-MM-DD)
    * TIMESTAMP - the date and time that the record was collected (YYYY-MM-DD HH:MM:SS)
    * RECORD.x - a unique identifying number provided from the data logger for table 1 from the original downloaded data. 
    * BattV - The battery voltage at the time of the sampling (Volts)
    * PTemp_C - 
    * Rain_mm_Tot - The total rainfall during the sampling interval (mm)
    * BP_mbar - The barometric pressure (mbar)
    * AirTC - the air temperature at the time of the sampling (dC)
    * RH - the relative humidity at the time of the sampling (%)
    * WS_ms - the wind speed at the time of the sampling event (m/s)
    * WindDir - the wind direction at the time of the sampling event (degrees from true N)
    * SlrW - the light flux density at the time of the sampling (W/m^2)
    * SlrMJ_Tot - the total light flux (MJ/m^2) 
    * CS320_Temp - 
    * DewPt_C - the dew point (dC)
    * RECORD.y - a unique identifying number provided from the data logger for table 1 from the original downloaded data.
    * BattV_Min - The minimum battery voltage at the time of the sampling (Volts)
