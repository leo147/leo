Water Pressure Metadata
"Plot Title: HS_Dock_Pressure_Trans"
"#","Date Time, GMT-04:00","Abs Pres, kPa (LGR S/N: 20844110, SEN S/N: 20844110)","Temp, °C (LGR S/N: 20844110, SEN S/N: 20844110)"
 Water Pressure Deploy Begin:
2021-08-14 13:30:00
Water Deploy End:
2021-09-18 14:05:00
Sensor Depth (cm)
19
Notes
There was no BP data collected on the site during the deployment, so the BP correction is using the mean barometric pressure for Richmond Va. from https://www.weatherwx.com/hazardoutlook/va/richmond.html
