# General Metadata for Chalgrove Lake YSI Data 

## Files

Specific metadata for each deployment can be found as text files with the file format of:

    BC_chalgrove_lake_YYYY-MM-DD_YYYY-MM-DD_metadata.txt
    
Where YYYY-MM-DD_YYYY-MM-DD is the date range of the deployment.

## File Created

  * 2019-04-12 by KF
  
## File Modified

  * 2019-10-31 by KF - added the descriptions of the total algae variables
  * 2022-05-04 by KF - updated to match LEO standards and current deployment specs
  * 2022-05-23 by KF - updated to include Total Algae Sensor information

## Description

These data are from a YSI EXO2 multiparameter sonde deployed at approximatly 0.7 m in Chalgrove Lake (37.242983 N, -78.464116 W) in approximately 4 m of water.

## Sonde Specifics

  The specific sensors and SNs deployed are on the metadata specific to each deployment but generally the sonde has:
  
  * Temperature
  * Conductivity
  * pH
  * ODO
  * Turbidity
  * Total Algae
  
## Measurement Parameters, units, and Variable Names

  * date.time = the combined date and time of the measurement (YYYY-MM-DD HH:MM:SS)
  * date = the sampling date reported by the YSI (M/D/YYYY)
  * time = the sampling time reported by the YSI (HH:MM:SS)
  * frac_sec = the sampling fractions of a second reported by the YSI
  * site_name = the site name on the YSI deployment (left blank)
  * Cond = the conductivity (uS/cm)
  * Chl_RFU = the raw fluorescence of the chlorophyll sensor (RFU)
  * nLF_Cond = the nLF conductivity (uS/cm)
  * DO_perc = the percent oxygen saturation
  * DO_percL = the percent local oxygen saturation
  * DO_mgL = the dissolved oxygen concentration (mg/L)
  * SAL = the salinity (psu)
  * SPC = the specific conductivity (uS/cm)
  * TAL_PC_RFU = the raw fluorescence of the phycocyanin (blue green algae) sensor (RFU)
  * TDS = the total dissolved solids (mg/L)
  * Turbidity = the turbidity of the water (FNU)
  * TSS = the total suspended solids
  * pH = the pH
  * pH_mV = the millivolts of the pH sensor
  * Temp = the temperature (dC)
  * Batt_V = the battery voltage
  * Cable_V = the cable power
  
