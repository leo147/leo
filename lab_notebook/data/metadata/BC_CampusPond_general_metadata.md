# General Metadata for Campus Pond Sampling Station

## Files

Specific metadata for each deployment and sensor can be found as text files with the file format of:

    BC_CampusPond_YYYY-MM-DD_YYYY-MM-DD_metadata.txt
    
Where YYYY-MM-DD_YYYY-MM-DD is the date range of the sampling period.

NOTE: The metadata in the above file is collected from the data logger and does not have all of fields present in the final data set, because some were created during data cleaning. Details on how the data were cleaned and variables created can be found at in the cleaning scripts on Gitlab [https://gitlab.com/leo147/leo/-/tree/master/lab_notebook/data_processing/cleaning_scripts](https://gitlab.com/leo147/leo/-/tree/master/lab_notebook/data_processing/cleaning_scripts).

## File Created

  * 2021-11-10 by KF - copied and modified from HS_wetland_general_metadata.md
  
## File Modified

## Description

These data are from the sampling station in Campus Pond on the Longwood University Campus. The sensors are along the S shoreline of the pond (37.296813, -78.397702).

All data are CC-BY and should be cited using the DOI available at https://zenodo.org/communities/leo/

## Station Specifics

  The specific sensors at the site are:

    * Water Temperature (dC) and Dissolved Oxygen (mg/l) are collected with a Onset HOBO U26-001 Dissolved Oxygen Logger
    * Water Temperature (dC) and Water Pressure (mmHg) are collected with an Onset HOBO U20-001-01 Water Level Logger
    * Water Temperature (dC) and Conductivity are collected with an Onset HOBO U24-001 Conductivity Logger
    * Air Temperature (dC) and Barometric Pressure (mmHg) are collected with an Onset HOBO U20-001-01 Water Level Logger mounted in the air next to the wetland.

The sensors are sampled every 15 minutes
 
## Measurement Parameters, units, and Variable Names

    * date.time - the date and time that the record was collected, reported in POSIX standard time (YYYY-MM-DD HH:MM:SS)
    * observation.DO, .CT, .press, or .BP - the incremental number of each observation from the DO, conductivity, water pressure, or barometric pressure sensor.
    * timestamp.DO, .CT, .press, or .BP - the data and time that the record was collected, as reported by the data logger (MM/DD/YY HH:MM:SS A/PM) from the DO, conductivity, water pressure, or barometric pressure sensor.
    * DO - the concentration of dissolved oxygen in the water (mg/L)
    * Temp.DO, .CT - the temperature (dC) from the DO or conductivity.
    * WaterTemp - the water temperature measured from the pressure transducer in the water (dC).
    * AirTemp - the air temperature measured from the pressure transducer (BP sensor) in the air (dC).
    * Pressure.press or .BP - the pressure recorded by the pressure transducer (kPa) on the water pressure or barometric pressure sensor.
    * Z - the depth of the water (cm).
    * Low_Range_CT - the conductivity read from 0 - 2500 uS/cm (uS/cm)
    * Full_Range_CT - the conductivity read from 0 - 15000 uS/cm (mmHg)
    * press.g.cm2 - the pressure from the water pressure sensor (g/cm^2)
    * BP.g.cm2 - the barometric pressure from the barometric pressure sensor (g/cm^2)

