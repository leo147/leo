﻿"Plot Title: LP_CampusPond_DO_raw"
"#","Date Time, GMT-05:00","DO conc, mg/L (LGR S/N: 20781165, SEN S/N: 20781165)","Temp, °C (LGR S/N: 20781165, SEN S/N: 20781165)","Coupler Attached (LGR S/N: 20781165)","Host Connected (LGR S/N: 20781165)","Stopped (LGR S/N: 20781165)","End Of File (LGR S/N: 20781165)"
Deploy Begin:
2022-02-17 14:30:00
Deploy End:
2022-03-14 18:41:00
ODO Calibration:
2021-11-03
Increased the `deploy.begin` time by 10 minutes after inspecting the plot of temperatuer. It looked like the first value was still an air temperature
