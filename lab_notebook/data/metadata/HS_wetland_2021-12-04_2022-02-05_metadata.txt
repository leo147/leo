notes
The sensor cap expired on 2021-11-29. After this both the temperature and the DO became -900.  I am using this date as the end of the deployment, even though the sensors were not removed from the water until 2021-12-04.
﻿"Plot Title: HS_wetland_DO_raw"
"#","Date Time, GMT-05:00","DO conc, mg/L (LGR S/N: 20781166, SEN S/N: 20781166)","Temp, °C (LGR S/N: 20781166, SEN S/N: 20781166)"
Deploy Begin:
2021-12-04 14:45:00
Deploy End:
2022-02-05 12:00:00
ODO Calibration:
2021-10-28
notes
 
﻿"Plot Title: HS_wetland_CT_raw"
"#","Date Time, GMT-05:00","Low Range, μS/cm (LGR S/N: 20781943, SEN S/N: 20781943)","Full Range, μS/cm (LGR S/N: 20781943, SEN S/N: 20781943)","Temp, °C (LGR S/N: 20781943, SEN S/N: 20781943)"
Deploy Begin:
2021-12-04 14:45:00
Deploy End:
2022-02-05 12:00:00
YSI SPC = 
67.4
Water Pressure Metadata
﻿"Plot Title: HS_wetland_pressure_trans_raw"
"#","Date Time, GMT-05:00","Abs Pres, kPa (LGR S/N: 20844112, SEN S/N: 20844112)","Temp, °C (LGR S/N: 20844112, SEN S/N: 20844112)"
 Water Pressure Deploy Begin:
2021-12-04 12:00:00
Water Deploy End:
2022-02-05 12:00:00
Reference Depth Time
needs to be measured
Reference Depth (m)
needs to be measured
BP Metadata
﻿"Plot Title: HS_wetland_BP_raw"
"#","Date Time, GMT-05:00","Abs Pres, kPa (LGR S/N: 10005125, SEN S/N: 10005125, LBL: Atm Pressure)","Temp, °C (LGR S/N: 10005125, SEN S/N: 10005125, LBL: Temp)"
BP Deploy Begin:
2021-12-04 12:00:00
BP Deploy End:
2022-02-05 12:00:00
Reference Depth Time
needs to be measured
Notes
The negative depth readings when the water level drops to 0 may be due to the fact that the BP sensor in the air is mounted approx 100 cm above the sediments of the wetland.
