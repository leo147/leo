# General Metadata for Chalgrove Lake Water Chemistry and Light Profile Data

## Files

These metadata are for the following files:

    BC_chalgrove_lake_water_chem.csv

    BC_chalgrove_lake_light.csv


## File Created

  * 2022-06-29 KF
  
## File Modified

## Description

These data are from water chemistry and light profiles collected from  Chalgrove Lake (37.242983 N, -78.464116 W) near the center of the lake in approximately 4 m of water. All profiles were collected at the LEO YSI anchor.

Prior to 2022-06-28 the water chemistry profiles were collected with a YSI 556 Multiparameter Handheld attached to a 600OMS V2 Optical Monitoring Sonde that measured temperature, conductivity and optical dissolved oxygen.

Data from 2022-06-28 forward were collected with a YSI ProODO handheld with a ProDIGITAL ODO/CT sensor.

All light data were collected with a LiCor LI-250A meter with a LI-192 underwater quantum sensor.

## Measurement Parameters, units, and Variable Names

  * date = the sampling date YYYY-MM-DD
  * Z = the dept that the data were collected (m)
  * temp = the temperature (dC)
  * perc_DO = the percent oxygen saturation
  * conc_DO = the dissolved oxygen concentration (mg/L)
  * SPC = the specific conductivity (uS/cm)
  * I = irradiance (umol/s/m^2)
  * secchi = the Secchi depth (m)
  * max_Z = the maximum depth at the YSI anchor measured with a handheld sonar (m)
  
