Water Pressure Metadata
"Plot Title: LP_CampusPond_PRESS_raw"
"#","Date Time, GMT-04:00","Abs Pres, kPa (LGR S/N: 20810903, SEN S/N: 20810903)","Temp, °C (LGR S/N: 20810903, SEN S/N: 20810903)","Coupler Detached (LGR S/N: 20810903)","Coupler Attached (LGR S/N: 20810903)","Host Connected (LGR S/N: 20810903)","Stopped (LGR S/N: 20810903)","End Of File (LGR S/N: 20810903)"
 Water Pressure Deploy Begin:
2021-11-03 14:30:00
Water Deploy End:
2021-11-23 11:11:00
Reference Depth Time
needs to be measured
Reference Depth (m)
needs to be measured
BP Metadata
"Plot Title: LP_CampusPond_BP_raw "
"#","Date Time, GMT-04:00","Abs Pres, kPa (LGR S/N: 20844113, SEN S/N: 20844113)","Temp, °C (LGR S/N: 20844113, SEN S/N: 20844113)","Coupler Detached (LGR S/N: 20844113)","Coupler Attached (LGR S/N: 20844113)","Host Connected (LGR S/N: 20844113)","Stopped (LGR S/N: 20844113)","End Of File (LGR S/N: 20844113)"
BP Deploy Begin:
2021-11-03 14:30:00
BP Deploy End:
2021-11-23 11:11:00
Reference Depth Time
needs to be measured
Notes
Increased the `deploy.begin` time by 10 minutes after inspecting the plot of temperatuer. It looked like the first value was still an air temperature
