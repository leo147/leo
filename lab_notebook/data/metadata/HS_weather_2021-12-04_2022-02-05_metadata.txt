Notes
The rainfall sensor is not functioning properly. The rainfall data were removed from the cleaned data using `Script 2'
"TOA5","HS_weather_CR300","CR300","1297","CR300.Std.07.02","CPU:HS_weather_2021-10-28.CR300","22482","Table1"
"TIMESTAMP","RECORD","Rain_mm_Tot","AirTC","BP_mbar","RH","SlrW","SlrMJ_Tot","WS_ms","WindDir"
"TS","RN","mm","Deg C","mbar","%","W/m^2","MJ/m^2","meters/second","degrees"
"","","Tot","Smp","Smp","Smp","Smp","Tot","Smp","Smp"
"TOA5","HS_weather_CR300","CR300","1297","CR300.Std.07.02","CPU:HS_weather_2021-10-28.CR300","22482","Table2"
"TIMESTAMP","RECORD","BattV_Min"
"TS","RN","Volts"
"","","Min"
