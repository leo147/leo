# General Metadata for Hull Springs Dock-YSI Sampling Station

## Files

Specific metadata for each deployment can be found as text files with the file format of:

    HS_YSI_YYYY-MM-DD_metadata.txt
    HS_dock_pressure_trans_YYYY-MM-DD_metadata.txt
    
Where YYYY-MM-DD is the date that the sampling period ended.

NOTE: The metadata in the above file is collected from the data logger and does not have all of fields present in the final data set, because some were created during data cleaning.

## File Created

  * 2021-07-14 by KF
  
## File Modified

  * 2021-07-22 by KF - added metadata for pressure transducer

## Description

These data are from the sampling station in Aimes Creek on the Camp House dock (38.125367, -76.659537).

    * Physical and chemical water data are collected with a YSI EXO2 sonde.

    * Temperature (dC) and Pressure (KPa) are collected with an Onset HOBO U20-001-01-Ti Water Level Logger

All data are CC-BY and should be cited using the DOI available at https://zenodo.org/communities/leo/

## Station Specifics

The sensors are sampled every 15 minutes
 
### Measurements Parameters, units, and Variable Names

* date.time - the date and time that the record was collected, reported in POSIX standard time (YYYY-MM-DD HH:MM:SS)
* date.time.adj - the date and time that the pressure was measured adjusted to match the time that barometric pressure was collected. (YYYY-MM-DD HH:MM:SS)
* observation - the number of the observation.
* timestamp - the date and time as reported by the data logger (MM/DD/YY HH:MM:SS AM/PM)
* date - the date that the record was collected (MM/DD/YYYY)
* time - the time that the record was collected (HH:MM:SS)
* site_name - the ID name provided by the KOR software.
* unit_ID - the sonde identification number.
* user_ID - the user that created the sampling template.
* Temp_dC - the water temperature in degrees C.
* DO_perc - the dissolved oxygen percent saturation (%).
* DO_percL - the "local" dissolved oxygen percent saturation where the calibration is locally always 100% saturated independent of the barometric pressure (%).
* DO_mg-L - the concentration of dissolved oxygen (mg/L).
* SPC_uS-cm - the specific conductance (uS/cm).
* C_uS-cm - the conductivity (uS/cm).
* nLFC_uS-cm
* TDS_mg-L - the total dissolved solids (mg/L).
* SAL_PSU - the salinity of the water (PSU).
* pH - the pH of the water.
* pH_mV - the millivolt measurement of the pH meter (mV).
* FNU - the turbidity of the water (FNU).
* TSS_mg-L - the total suspended solids (mg/L).
* BGA_PC_RFU - the phycocyanin fluorescence level which is an indicator of blue-green algae (RFU).
* BGC_PC_ug-L - the concentration of phycocyanin in the water, which is an indicator of blue-green algae (ug/L).
* Chl_RFU - the chlorophyll fluorescence level, which is an indicator of phytoplankton biomass (RFU).
* Chl_ug-L - the concentration of chlorophyll in the water, which is an indicator of phytoplankton biomass (ug/L).
* fDOM_RFU - the fluorescent DOM fluorescence level (RFU).
* fDOM_QSU - the standardized fluorescent DOM concentration (QSU).
* Wiper_V - the voltage output of the wiper (V).
* Cabel_V - the voltage output of the cable (V).
* Batt_V - the voltage output of the internal batteries (V).
* Pressure - the pressure of the water above the pressure transducer (KPa)
* Temp - the water temperature reported by the pressure transducer (dC)
* press_mmHg - the pressure of the water above the pressure transducer (mm Hg)
* BP_mmHg - the barometric pressure recorded by the weather station at the Yellow House (mm Hg)
* Z_press_trans - the depth of the water above the pressure transducer (cm)
* Z - the depth of the water above the sediments (cm)

