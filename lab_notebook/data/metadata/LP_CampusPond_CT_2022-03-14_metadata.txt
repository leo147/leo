﻿"Plot Title: LP_CampusPond_CT_raw"
"#","Date Time, GMT-05:00","Low Range, μS/cm (LGR S/N: 20781942, SEN S/N: 20781942)","Full Range, μS/cm (LGR S/N: 20781942, SEN S/N: 20781942)","Temp, °C (LGR S/N: 20781942, SEN S/N: 20781942)","Coupler Detached (LGR S/N: 20781942)","Coupler Attached (LGR S/N: 20781942)","Host Connected (LGR S/N: 20781942)","Stopped (LGR S/N: 20781942)","End Of File (LGR S/N: 20781942)"
Deploy Begin:
2022-02-17 14:30:00
Deploy End:
2022-03-14 18:41:00
YSI SPC = 
No SCP measured
Increased the `deploy.begin` time by 10 minutes after inspecting the plot of temperature. It looked like the first value was still an air temperature
