# This code is for the analysis of the Chalgrove Lake YSI data

## Description

The YSI data from Chalgrove Lake can be used to understand the metabolic processes of the lake. These analyses are intended to describe the patterns in DO and other variables and relate them to patterns in metabolism.

## Install packages

    library("tidyverse")

## Metadata

* Copied and modified from a file created 2023-02-02 by LE: 2023-03-08 - KF

## Import Data
    
    NDJ <- read.csv("./data/BC_chalgrove_lake_2022-11-04_2023-01-19.csv", header = T)
    SO <- read.csv("./data/BC_Chalgrove_lake_2022-09-20_2022-10-17.csv", header = T)
    AS <- read.csv("./data/BC_chalgrove_lake_2022-08-05_2022-09-19.csv", header = T)
    JJA <- read.csv("./data/BC_chalgrove_lake_2022-06-28_2022-08-05.csv", header = T)    
    MJ <- read.csv("./data/BC_chalgrove_lake_2022-05-20_2022-06-28.csv", header = T)    
    MAM <- read.csv("./data/BC_chalgrove_lake_2022-03-22_2022-05-18.csv", header = T)
    FM <- read.csv("./data/BC_chalgrove_lake_2022-02-18_2022-03-18.csv", header = T)
    
## Format date
    
    AS$date.time <- as.POSIXct(AS$date.time)
    AS$date <- as.POSIXct(
        AS$date,
        format = "%m/%d/%Y")
    
    FM$date.time <- as.POSIXct(FM$date.time)
    FM$date <- as.POSIXct(
        FM$date,
        format = "%m/%d/%Y")
    
    JJA$date.time <- as.POSIXct(JJA$date.time)
    JJA$date <- as.POSIXct(
        JJA$date,
        format = "%m/%d/%Y")
    
    MJ$date.time <- as.POSIXct(MJ$date.time)
    MJ$date <- as.POSIXct(
        MJ$date,
        format = "%m/%d/%Y")
    
    MAM$date.time <- as.POSIXct(MAM$date.time)
    MAM$date <- as.POSIXct(
        MAM$date,
        format = "%m/%d/%Y")
    
    SO$date.time <- as.POSIXct(SO$date.time)
    SO$date <- as.POSIXct(
        SO$date,
        format = "%m/%d/%Y")
    
    NDJ$date.time <- as.POSIXct(NDJ$date.time)
    NDJ$date <- as.POSIXct(
        NDJ$date,
        format = "%m/%d/%Y")
    
## Isolate and standardize variables in the data.frames
### Variables to Isolate date.time, date, time, Cond, DO_perc, DO_mgL, Turbidity, pH, Temp)
    
    AS.stand <- AS[, c(
    	1,
    	2,
    	3,
    	8,
    	10,
    	12,
    	18,
    	20,
    	22
    )
    ]
    
    FM.stand <- FM[, c(
    	1,
    	2,
    	3,
    	6,
    	8,
    	10,
    	14,
    	16,
    	18
    )
    ]
    
    
    JJA.stand <- JJA[, c(
    	1,
    	2,
    	3,
    	7,
    	9,
    	11,
    	16,
    	18,
    	20
    )
    ]
    
    MAM.stand <- MAM[, c(
    	1,
    	2,
    	3,
    	7,
    	9,
    	11,
    	16,
    	18,
    	20
    )
    ]
    
    MJ.stand <- MJ[, c(
    	1,
    	2,
    	3,
    	7,
    	9,
    	11,
    	16,
    	18,
    	20
    )
    ]
    
    NDJ.stand <- NDJ[, c(
    	1,
    	2,
    	3,
    	8,
    	10,
    	12,
    	18,
    	20,
    	22
    )
    ]
    
    SO.stand <- SO[, c(
    	1,
    	2,
    	3,
    	7,
    	9,
    	11,
    	16,
    	18,
    	20
    )
    ]
    
### Merge the standardized data.frames
    
    foo <- full_join(
    	AS.stand,
    	FM.stand
    )
    
    foo <- full_join(
    	foo, 
    	JJA.stand
    )
    
    foo <- full_join(
    	foo, 
    	MAM.stand
    )
    	
    foo <- full_join(
    	foo, 
    	MJ.stand
    )
    
    foo <- full_join(
    	foo, 
    	NDJ.stand
    )
    
    CL <- full_join(
    	foo,
    	SO.stand
    )
    
## Create Month Variable
    
    year.month <- format(
      CL$date.time,
    	"%Y-%B"
    )
    
    year.month <- factor(
    	year.month,
    	levels = c(
    		"2022-February",
    		"2022-March",
    		"2022-April",
    		"2022-May",
    		"2022-June",
    		"2022-July",
    		"2022-August",
    		"2022-September",
    		"2022-October",
    		"2022-November",
    		"2022-December",
    		"2023-January"
    	)
    )

### Add year.month variable to CL data.frame
    
    CL <- data.frame(
    	year.month,
    	CL
    )
    
### Remove the NA's from CL
    
There are 4 rows of NAs in all the variables that need to be removed

    CL <- CL[complete.cases(CL), ] 
    	
    
## Analyze Data with plots
    
    ggplot(CL, mapping = aes(
               x = date.time,
               y = Temp,
    	         color = Temp
           )
    )+
        geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
      labs(
        x = " ",
        y = "Water Temperature (dC)",
        color = "Temperature"
        )+
      theme_dark(
      	base_size = 15
      	)
    
     ggsave(
     	"./data_analysis/plots/CL_Temp_by_date.jpeg",
     	dpi = 400
     )
     
    ggplot(CL, mapping = aes(
               x = date.time,
               y = DO_mgL,
    	         color = Temp
           )
    )+
        geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
    	labs(
    		x = " ",
    		y = "Dissolved Oxygen Concentration (mg/L)",
    		color = "Temp. (dC)"
    	)+
      theme_dark(
      	base_size = 15
      	)
    
     ggsave(
     	"./data_analysis/plots/CL_DOconc_by_date.jpeg",
     	dpi = 400
     )
     
    ggplot(CL, mapping = aes(
      x = date.time,
      y = DO_perc,
      color = Temp
    )
    )+
      geom_point()+
      scale_colour_distiller(
        palette = "Spectral"
      )+
    	labs(
    		x = " ",
    		y = "Dissolved Oxygen Percent Saturation",
    		color = "Temp (dC)"
    	)+
      theme_dark(
      	base_size = 15
      	)
    
     ggsave(
     	"./data_analysis/plots/CL_DOperc_by_date.jpeg",
     	dpi = 400
     )
     
    ggplot(CL, mapping = aes(
               x = date.time,
               y = Cond,
    	         color = Temp
           )
    )+
        geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
    	labs(
    		x = " ",
    		y = "Conductivity (meq/L)",
    		color = "Temp (dC)"
    	)+
      theme_dark(
      	base_size = 15
      	)

     ggsave(
     	"./data_analysis/plots/CL_Cond_by_date.jpeg",
     	dpi = 400
     )
     
    ggplot(CL, mapping = aes(
               x = date.time,
               y = Turbidity,
    	         color = Temp
           )
    )+
        geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
      theme_dark()
    
    # the pH data is incorrect due to a bad sensor after 2022-11-01 
    ggplot(subset(CL, date < "2022-11-01"), mapping = aes(
               x = date.time,
               y = pH,
    	         color = Temp
           )
    )+
        geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
    	labs(
    		x = " ",
    		y = "pH",
    		color = "Temp. (dC)"
    	)+
      theme_dark(
      	base_size = 15
      	)
    
     ggsave(
     	"./data_analysis/plots/CL_pH_by_date.jpeg",
     	dpi = 400
     )
    
    ggplot(subset(CL, date < "2022-11-01"), mapping = aes(
               x = Temp,
               y = pH,
    	         color = Temp
           )
    )+
        geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
      theme_dark()
    
    ggplot(subset(CL, date < "2022-11-01"), mapping = aes(
    	         x = date.time,
               y = pH,
    	         color = DO_perc
           )
    )+
        geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
      theme_dark()
    
    ggplot(subset(CL, date < "2022-11-01") , mapping = aes(
    	         x = DO_perc,
               y = pH,
    	         color = Temp
           )
    )+
        geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
      theme_dark()

    ggplot(subset(CL, date < "2022-11-01") , mapping = aes(
    	         x = DO_mgL,
               y = pH,
    	         color = Temp
           )
    )+
        geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
    	labs(
    		x = "Dissolved Oxygen Concentration (mg/L)",
    		y = "pH",
    		color = "Temp. (dC)"
    	)+
      theme_dark(
      	base_size = 15
      	)

     ggsave(
     	"./data_analysis/plots/CL_pH_by_DO.jpeg",
     	dpi = 400
     )
     
### Analyze Data by Month
#### Plots by time
    
    ggplot(subset(
    	CL, 
    	year.month == "2022-September"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
    	ylim(50, 120)
    
    ggplot(subset(
    	CL, 
    	year.month == "2022-March"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(50, 120)
    
    ggplot(subset(
    	CL, 
    	year.month == "2022-April"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	scale_colour_distiller(
    		palette = "Spectral"
    	)+
    	ylim(50, 120)
    
    ggplot(subset(
    	CL, 
    	year.month == "2022-May"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(50, 120)

    ggplot(subset(
    	CL, 
    	year.month == "2022-June"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(50, 120)

    ggplot(subset(
    	CL, 
    	year.month == "2022-July"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(0, 130)

    ggplot(subset(
    	CL, 
    	year.month == "2022-August"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(0, 130)

    ggplot(subset(
    	CL, 
    	year.month == "2022-September"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(0, 130)

    ggplot(subset(
    	CL, 
    	year.month == "2022-October"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(0, 130)
    
    ggplot(subset(
    	CL, 
    	year.month == "2022-November"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(0, 130)
    
    ggplot(subset(
    	CL, 
    	year.month == "2022-December"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(0, 130)
    
    ggplot(subset(
    	CL, 
    	year.month == "2023-January"
    ), mapping = aes(
    	x = date.time,
    	y = DO_perc,
    	color = Temp
    )
    )+
    	geom_point()+
    	ylim(0, 130)
    
#### Violin plots
    
     ggplot(CL, mapping = aes(
    	x = year.month,
    	y = Temp,
    	fill = year.month
    )
    )+
    	geom_violin(
    		width = 1.5 
    			)+
    	stat_summary(
    		fun = mean,
    		color = "black",
    		size = 0.2
    		)+
    	scale_fill_manual(
      		values = colorRampPalette(
      			c("blue", "green", "orange", "blue")
      			)(12)
    	)+
    	labs(
    		y = "Temperature (dC)",
    		fill = "Year-Month"
    	)+
    	theme_dark(
    		base_size = 15
    	)+
    	theme(
    		axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()
    		)
     
     ggplot(CL, mapping = aes(
    	x = year.month,
    	y = Cond,
    	fill = year.month
    )
    )+
    	geom_violin(
    		width = 1.5 
    			)+
    	stat_summary(
    		fun = mean,
    		color = "black",
    		size = 0.2
    		)+
    	scale_fill_manual(
      		values = colorRampPalette(
      			c("blue", "green", "orange", "blue")
      			)(12)
    	)+
    	labs(
    		y = "Conductivity (meq/L)",
    		fill = "Year-Month"
    	)+
    	theme_dark(
    		base_size = 15
    	)+
    	theme(
    		axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()
    		)
     
     ggplot(CL, mapping = aes(
    	x = year.month,
    	y = DO_mgL,
    	fill = year.month
    )
    )+
    	geom_violin(
    		width = 1.5 
    			)+
    	stat_summary(
    		fun = mean,
    		color = "black",
    		size = 0.2
    		)+
    	scale_fill_manual(
      		values = colorRampPalette(
      			c("blue", "green", "orange", "blue")
      			)(12)
    	)+
    	labs(
    		y = "Dissolved Oxygen Concentration (mg/L)",
    		fill = "Year-Month"
    	)+
    	theme_dark(
    		base_size = 15
    	)+
    	theme(
    		axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
    		)
     
     ggsave(
     	"./data_analysis/plots/CL_DOconc_by_month_violin.jpeg",
     	dpi = 400
     )
   
    ggplot(CL, mapping = aes(
    	x = year.month,
    	y = DO_perc,
    	fill = year.month
    )
    )+
    	geom_violin(
    		width = 1.5 
    			)+
    	stat_summary(
    		fun = mean,
    		color = "black",
    		size = 0.2
    		)+
    	scale_fill_manual(
      		values = colorRampPalette(
      			c("blue", "green", "orange", "blue")
      			)(12)
    	)+
    	labs(
    		y = "Percent Dissolved Oxygen Saturation",
    		fill = "Year-Month"
    	)+
    	theme_dark(
    		base_size = 15
    	)+
    	theme(
    		axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()
    		)
    
     ggsave(
     	"./data_analysis/plots/CL_DOperc_by_month_violin.jpeg",
     	dpi = 400
     )
     
    ggplot(subset(CL, date < "2022-11-01"), mapping = aes(
    	x = year.month,
    	y = pH,
    	fill = year.month
    )
    )+
    	geom_violin(
    		width = 1.5 
    			)+
    	stat_summary(
    		fun = mean,
    		color = "black",
    		size = 0.2
    		)+
    	scale_fill_manual(
      		values = colorRampPalette(
      			c("blue", "green", "orange")
      			)(9)
    	)+
    	labs(
    		y = "pH",
    		fill = "Year-Month"
    	)+
    	theme_dark(
    		base_size = 15
    	)+
    	theme(
    		axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()
    		)
    
     ggsave(
     	"./data_analysis/plots/CL_pH_by_month_violin.jpeg",
     	dpi = 400
     )
     
## Calculate percent of days with certain conditions
### percent of days with DO_perc > 100
    
    n.super.sat <- length(
    	CL$DO_perc[
    		CL$DO_perc >= 100
    	]
    )
    
    n.hypoxic <- length(
    	CL$DO_perc[
    		CL$DO_perc <= 30
    	]
    )

    (tot.perc.obs.super.sat <- (n.super.sat / length(CL$DO_perc) ) * 100)
    
    (tot.perc.obs.hypoxic <- (n.hypoxic / length(CL$DO_mgL) ) * 100)
    
    (perc.obs.super.sat <- CL %>%
    	group_by(
    		year.month
    	) %>%
    	summarize(
    		perc.super.sat = (length(DO_perc) / n.super.sat) * 100,
    		perc.under.sat = 100 - (length(DO_perc) / n.super.sat) * 100
    	)
    )

### Plot perc.super.sat
    
    ggplot(perc.obs.super.sat, mapping = aes(
    	x = year.month,
    	y = perc.super.sat
    )
    )+
    	geom_point()
    