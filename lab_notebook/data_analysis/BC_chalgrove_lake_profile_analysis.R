# Chalgrove Lake Profile Analysis
## Metadata 
* Created 2022-10-20 LE
* Modified 2022-03-16 - KF - added an additional profile and updated code for figures


# Description

  library("tidyverse")

# Input data

    chem <- read.csv("./data/BC_chalgrove_lake_water_chem.csv", header = T)
    light <- read.csv("./data/BC_chalgrove_lake_light.csv", header = T)    
    
# Convert to POSIX
    
    chem$date <- as.POSIXct(chem$date)
    # chem$time <- as.POSIXct(chem$time)    
    light$Date <- as.POSIXct(light$Date)

# Create variables
    
    lnI <- log(light$I)

    light <- data.frame(light, lnI)
    
# Calculate k
## 2022-03-18
### Plot data
    
    plot(lnI ~ Z, data = light, subset = Date == "2022-03-18")
    plot(I ~ Z, data = light, subset = Date == "2022-03-18", type = "l")
    plot(lnI ~ Z, data = light, subset = Date == "2022-03-18", type = "l")
    
### Calc k
    
    summary(lm(lnI ~ Z, data = light, subset = Date == "2022-03-18"))    
    
# Calculate k
## 2022-03-18
### Plot data
    
    plot(lnI ~ Z, data = light, subset = Date == "2022-05-18")
    plot(I ~ Z, data = light, subset = Date == "2022-05-18", type = "l")
    plot(lnI ~ Z, data = light, subset = Date == "2022-05-18", type = "l")
    
### Calc k
    
    summary(lm(lnI ~ Z, data = light, subset = Date == "2022-03-18"))
    
    summary(lm(lnI ~ Z, data = light, subset = Date == "2022-05-18"))
    
    summary(lm(lnI ~ Z, data = light, subset = Date == "2022-06-28"))
    
    summary(lm(lnI ~ Z, data = light, subset = Date == "2022-08-05"))
    
    summary(lm(lnI ~ Z, data = light, subset = Date == "2022-09-20"))
    
    summary(lm(lnI ~ Z, data = light, subset = Date == "2022-11-04"))
    
## Plot photic zone depth
### calculate % light
    
    perc.I.2022.03.18 <- 
      (light$I[light$Date == "2022-03-18"] / max(light$I[light$Date == "2022-03-18"])) * 100
    
    plot(perc.I.2022.03.18 ~ light$Z[light$Date == "2022-03-18"], type = "l", ylim = c(0, 100), xlim = c(0, 1.5), col = "blue")
    
    perc.I.2022.05.18 <- 
      (light$I[light$Date == "2022-05-18"] / max(light$I[light$Date == "2022-05-18"])) * 100

    points(perc.I.2022.05.18 ~ light$Z[light$Date == "2022-05-18"], type = "l", col = "purple")
    
    perc.I.2022.06.28 <- 
      (light$I[light$Date == "2022-06-28"] / max(light$I[light$Date == "2022-06-28"])) * 100
    
    points(perc.I.2022.06.28 ~ light$Z[light$Date == "2022-06-28"], type = "l", col = "green")
    
    perc.I.2022.08.05 <- 
      (light$I[light$Date == "2022-08-05"] / max(light$I[light$Date == "2022-08-05"])) * 100
    
    points(perc.I.2022.08.05 ~ light$Z[light$Date == "2022-08-05"], type = "l", col = "orange")
    
    perc.I.2022.09.20 <- 
      (light$I[light$Date == "2022-09-20"] / max(light$I[light$Date == "2022-09-20"])) * 100
    
    points(perc.I.2022.09.20 ~ light$Z[light$Date == "2022-09-20"], type = "l", col = "red")
    
    perc.I.2022.11.04 <- 
      (light$I[light$Date == "2022-11-04"] / max(light$I[light$Date == "2022-11-04"])) * 100
    
    points(perc.I.2022.11.04 ~ light$Z[light$Date == "2022-11-04"], type = "l", col = "red")
    
    perc.I.2023.01.19 <- 
      (light$I[light$Date == "2023-01-19"] / max(light$I[light$Date == "2023-01-19"])) * 100
    
    perc.I.2023.03.07 <- 
      (light$I[light$Date == "2023-03-07"] / max(light$I[light$Date == "2023-03-07"])) * 100
    
    abline(h = 0)
    abline(h = 1, lty = 2)
   
### plot of the percent surface light reaching the sediments
    
    ggplot()+
      geom_line(subset(
        light, 
        Date == "2022-03-18"
        ), 
        mapping = aes(
        x = Z,
        y = perc.I.2022.03.18
        )
      )+
      geom_line(subset(
        light, 
        Date == "2022-05-18"
        ), 
        mapping = aes(
        x = Z,
        y = perc.I.2022.05.18
        ),
        color = "orange"
      )+
      geom_line(subset(
        light, 
        Date == "2022-06-28"
        ), 
        mapping = aes(
        x = Z,
        y = perc.I.2022.06.28
        ),
        color = "green"
      )
# Chemistry Plot for 2022-03-18
    
    ggplot(subset(chem, date == "2022-03-18"),
      aes(
        x = temp,
        y = depth
      )
      )+
          geom_point()+
          geom_line()+
          scale_y_reverse()
    
    ggplot(chem,
      aes(
        x = temp,
        y = depth,
        color = date
      )
      )+
          geom_point(
            size = 3
            )+
          #geom_line()+
          scale_y_reverse()
    
    ggplot(chem,
      aes(
        x = perc_DO,
        y = depth,
        color = date
      )
      )+
          geom_point(
            size = 3
            )+
          geom_line()+
          scale_y_reverse()
    
    ggplot(chem,
      aes(
        x = conc_DO,
        y = depth,
        color = date
      )
      )+
          geom_point(
            size = 3
            )+
          #geom_line()+
          scale_y_reverse()
    
### Line graphs
    
    # ggplot concentrated DO
    
    ggplot()+
      #geom_line(subset(chem, date == "2022-03-18"), mapping = 
      #  aes(
      #    x = conc_DO,
      #    y = depth,
      #  	color = "2022-03-18"
      #  ),
      #  size = 1
      #)+
      geom_point(subset(chem, date == "2022-03-18"), mapping = 
                  aes(
                    x = conc_DO,
                    y = depth,
                  	color = "2022-03-18"
                  ),
                size = 3
          )+
      #geom_line(subset(chem, date == "2022-05-18"), mapping = 
      #            aes(
      #              x = conc_DO,
      #              y = depth,
      #            	color = "2022-05-18"
      #            ),
      #          size = 1
      #)+
      geom_point(subset(chem, date == "2022-05-18"), mapping = 
                  aes(
                    x = conc_DO,
                    y = depth,
                  	color = "2022-05-18"
                  ),
                size = 3
      )+
      #geom_line(subset(chem, date == "2022-06-28"), mapping = 
      #            aes(
      #              x = conc_DO,
      #              y = depth,
      #            	color = "2022-06-28"
      #            ),
      #          size = 1
      #)+
      geom_point(subset(chem, date == "2022-06-28"), mapping = 
                  aes(
                    x = conc_DO,
                    y = depth,
                  	color = "2022-06-28"
                  ),
                size = 3
      )+
      #geom_line(subset(chem, date == "2022-08-05"), mapping = 
      #            aes(
      #              x = conc_DO,
      #              y = depth,
      #            	color = "2022-08-05"
      #            ),
      #          size = 1
      #)+
      geom_point(subset(chem, date == "2022-08-05"), mapping = 
                  aes(
                    x = conc_DO,
                    y = depth,
                  	color = "2022-08-05"
                  ),
                size = 3
      )+
      #geom_line(subset(chem, date == "2022-09-20"), mapping = 
      #            aes(
      #              x = conc_DO,
      #              y = depth,
      #            	color = "2022-09-20"
      #            ),
      #          size = 1
      #)+
      geom_point(subset(chem, date == "2022-09-20"), mapping = 
                   aes(
                     x = conc_DO,
                     y = depth,
                   	color = "2022-09-20"
                   ),
                 size = 3
      )+
      #geom_line(subset(chem, date == "2022-11-04"), mapping = 
      #            aes(
      #              x = conc_DO,
      #              y = depth,
      #            	color = "2022-11-04"
      #            ),
      #          size = 1
      #)+
      geom_point(subset(chem, date == "2022-11-04"), mapping = 
                   aes(
                     x = conc_DO,
                     y = depth,
                   	color = "2022-11-04"
                   ),
                 size = 3
      )+
      scale_y_reverse()+
    	xlab("Dissolved Oxygen Conc. (mg/L)")+
    	ylab("Depth (m)")+
    	scale_color_brewer(
    		"Date",
    		palette = "GnBu"
    	)+
    	theme_dark(
    		base_size = 15
    		)
    
## Save Plot
    
    ggsave("./data_analysis/plots/BC_CL_DO_Z.jpg", dpi = 400)
 
    # ggplot concentrated DO with depth on the x-axis to fix the error in plotting the higher DO at 1 m.
    
    ggplot()+
      geom_line(subset(chem, date == "2022-03-18"), mapping = 
        aes(
          y = conc_DO,
          x = depth,
        	color = "2022-03-18"
        ),
        size = 1
      )+
      geom_point(subset(chem, date == "2022-03-18"), mapping = 
                  aes(
                    y = conc_DO,
                    x = depth,
                  	color = "2022-03-18"
                  ),
                size = 3
          )+
      geom_line(subset(chem, date == "2022-05-18"), mapping = 
                  aes(
                    y = conc_DO,
                    x = depth,
                  	color = "2022-05-18"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-05-18"), mapping = 
                  aes(
                    y = conc_DO,
                    x = depth,
                  	color = "2022-05-18"
                  ),
                size = 3
      )+
      geom_line(subset(chem, date == "2022-06-28"), mapping = 
                  aes(
                    y = conc_DO,
                    x = depth,
                  	color = "2022-06-28"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-06-28"), mapping = 
                  aes(
                    y = conc_DO,
                    x = depth,
                  	color = "2022-06-28"
                  ),
                size = 3
      )+
      geom_line(subset(chem, date == "2022-08-05"), mapping = 
                  aes(
                    y = conc_DO,
                    x = depth,
                  	color = "2022-08-05"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-08-05"), mapping = 
                  aes(
                    y = conc_DO,
                    x = depth,
                  	color = "2022-08-05"
                  ),
                size = 3
      )+
      geom_line(subset(chem, date == "2022-09-20"), mapping = 
                  aes(
                    y = conc_DO,
                    x = depth,
                  	color = "2022-09-20"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-09-20"), mapping = 
                   aes(
                     y = conc_DO,
                     x = depth,
                   	color = "2022-09-20"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-11-04"), mapping = 
                  aes(
                    y = conc_DO,
                    x = depth,
                  	color = "2022-11-04"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-11-04"), mapping = 
                   aes(
                     y = conc_DO,
                     x = depth,
                   	color = "2022-11-04"
                   ),
                 size = 3
      )+
    	xlab("Depth (m)")+
    	ylab("Dissolved Oxygen Conc. (mg/L)")+
    	scale_color_brewer(
    		"Date",
    		palette = "GnBu"
    	)+
    	theme_dark(
    		base_size = 15
    		)
    
# ggplot percent DO   
    
    ggplot()+
      geom_line(subset(chem, date == "2022-03-18"), mapping = 
                  aes(
                    x = perc_DO,
                    y = depth,
                  	color = "2022-03-18"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-03-18"), mapping = 
                   aes(
                     x = perc_DO,
                     y = depth,
                   	color = "2022--3-18"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-05-18"), mapping = 
                  aes(
                    x = perc_DO,
                    y = depth,
                  	color = "2022-05-18"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-05-18"), mapping = 
                   aes(
                     x = perc_DO,
                     y = depth,
                   	color = "2022-05-18"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-06-28"), mapping = 
                  aes(
                    x = perc_DO,
                    y = depth,
                  	color = "2022-06-28"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-06-28"), mapping = 
                   aes(
                     x = perc_DO,
                     y = depth,
                   	color = "2022-06-28"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-08-05"), mapping = 
                  aes(
                    x = perc_DO,
                    y = depth,
                  	color = "2022-08-05"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-08-05"), mapping = 
                   aes(
                     x = perc_DO,
                     y = depth,
                   	color = "2022-08-05"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-09-20"), mapping = 
                  aes(
                    x = perc_DO,
                    y = depth,
                  	color = "2022-09-20"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-09-20"), mapping = 
                   aes(
                     x = perc_DO,
                     y = depth,
                   	color = "2022-09-20"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-11-04"), mapping = 
                aes(
                  x = perc_DO,
                  y = depth,
                	color = "2022-11-04"
                ),
              size = 1
      )+
      geom_point(subset(chem, date == "2022-11-04"), mapping = 
                 aes(
                   x = perc_DO,
                   y = depth,
                 	color = "2022-11-04"
                 ),
               size = 3
      )+
      geom_vline(
        xintercept = 100,
      	linetype = 3,
      	size = 1
      )+
     scale_y_reverse()+
    	xlab("Percent Dissolved Oxygen")+
    	ylab("Depth (m)")+
    	scale_color_brewer(
    		"Date",
    		palette = "GnBu"
    	)+
    	theme_dark(
    		base_size = 15
    		)
    
## Save Plot
    
    ggsave("./data_analysis/plots/BC_CL_percDO_Z.jpg", dpi = 400)

# ggplot temperature 
       
    ggplot()+
      geom_line(subset(chem, date == "2022-03-18"), mapping = 
                  aes(
                    x = temp,
                    y = depth,
                    color = "2022-03-18"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-03-18"), mapping = 
                   aes(
                     x = temp,
                     y = depth,
                     color = "2022-03-18"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-05-18"), mapping = 
                  aes(
                    x = temp,
                    y = depth,
                    color = "2022-05-18"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-05-18"), mapping = 
                   aes(
                     x = temp,
                     y = depth,
                     color = "2022-05-18"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-06-28"), mapping = 
                  aes(
                    x = temp,
                    y = depth,
                    color = "2022-06-28"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-06-28"), mapping = 
                   aes(
                     x = temp,
                     y = depth,
                     color = "2022-06-28"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-08-05"), mapping = 
                  aes(
                    x = temp,
                    y = depth,
                    color = "2022-08-05"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-08-05"), mapping = 
                   aes(
                     x = temp,
                     y = depth,
                     color = "2022-08-05"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-09-20"), mapping = 
                  aes(
                    x = temp,
                    y = depth,
                    color = "2022-09-20"
                  ),
                size = 1
      )+
      geom_point(subset(chem, date == "2022-09-20"), mapping = 
                   aes(
                     x = temp,
                     y = depth,
                     color = "2022-09-20"
                   ),
                 size = 3
      )+
      geom_line(subset(chem, date == "2022-11-04"), mapping = 
                 aes(
                   x = temp,
                   y = depth,
                   color = "2022-11-04"
                 ),
               size = 1
      )+
      geom_point(subset(chem, date == "2022-11-04"), mapping = 
                 aes(
                   x = temp,
                   y = depth,
                   color = "2022-11-04"
                 ),
               size = 3
    )+
     scale_y_reverse()+
    	xlab("Temperature (dC)")+
    	ylab("Depth (m)")+
    	scale_color_brewer(
    		"Date",
    		palette = "GnBu"
    	)+
    	theme_dark(
    		base_size = 15
    		)
      
## Save Plot
    
    ggsave("./data_analysis/plots/BC_CL_Temp_Z.jpg", dpi = 400)
    
# ggplot irradiance
    
    ggplot()+
      geom_line(subset(light, Date == "2022-03-18"), mapping =
          
                  aes(
                    x = I,
                    y = Z,
                    color = "2022-03-18"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-03-18"), mapping = 
                   aes(
                     x = I,
                     y = Z,
                     color = "2022-03-18"
                   ),
                 size = 3
      )+
      geom_line(subset(light, Date == "2022-05-18"), mapping = 
                  aes(
                    x = I,
                    y = Z,
                    color = "2022-05-18"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-05-18"), mapping = 
                   aes(
                     x = I,
                     y = Z,
                     color = "2022-05-18"
                   ),
                 size = 3
      )+
      geom_line(subset(light, Date == "2022-06-28"), mapping = 
                  aes(
                    x = I,
                    y = Z,
                    color = "2022-06-28"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-06-28"), mapping = 
                   aes(
                     x = I,
                     y = Z,
                     color = "2022-06-28"
                   ),
                 size = 3
      )+
      geom_line(subset(light, Date == "2022-08-05"), mapping = 
                  aes(
                    x = I,
                    y = Z,
                    color = "2022-08-05"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-08-05"), mapping = 
                   aes(
                     x = I,
                     y = Z,
                     color = "2022-08-05"
                   ),
                 size = 3
      )+
      geom_line(subset(light, Date == "2022-09-20"), mapping = 
                  aes(
                    x = I,
                    y = Z,
                    color = "2022-09-20"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-09-20"), mapping = 
                   aes(
                     x = I,
                     y = Z,
                     color = "2022-09-20"
                   ),
                 size = 3
      )+
      geom_line(subset(light, Date == "2022-11-04"), mapping = 
                 aes(
                   x = I,
                   y = Z,
                   color = "2022-11-04"
                 ),
               size = 1
      )+
      geom_point(subset(light, Date == "2022-11-04"), mapping = 
                 aes(
                   x = I,
                   y = Z,
                   color = "2022-11-04"
                 ),
               size = 3
    )+
       scale_y_reverse()+
    	xlab("Irradiance")+
    	ylab("Depth (m)")+
    	scale_color_brewer(
    		"Date",
    		palette = "GnBu"
    	)+
    	theme_dark(
    		base_size = 15
    		)
    
## Save Plot
    
    ggsave("./data_analysis/plots/BC_CL_I_Z.jpg", dpi = 400)

# ggplot percent irradiance
    
    ggplot()+
      geom_line(subset(light, Date == "2022-03-18"), mapping =
                  aes(
                    x = perc.I.2022.03.18,
                    y = Z,
                    color = "2022-03-18"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-03-18"), mapping = 
                   aes(
                     x = perc.I.2022.03.18,
                     y = Z,
                     color = "2022-03-18"
                   ),
                 size = 3
      )+
     geom_line(subset(light, Date == "2022-05-18"), mapping = 
                  aes(
                    x = perc.I.2022.05.18,
                    y = Z,
                    color = "2022-05-18"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-05-18"), mapping = 
                   aes(
                     x = perc.I.2022.05.18,
                     y = Z,
                     color = "2022-05-18"
                   ),
                 size = 3
      )+
      geom_line(subset(light, Date == "2022-06-28"), mapping = 
                  aes(
                    x = perc.I.2022.06.28,
                    y = Z,
                    color = "2022-06-28"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-06-28"), mapping = 
                   aes(
                     x = perc.I.2022.06.28,
                     y = Z,
                     color = "2022-06-28"
                   ),
                 size = 3
      )+
      geom_line(subset(light, Date == "2022-08-05"), mapping = 
                  aes(
                    x = perc.I.2022.08.05,
                    y = Z,
                    color = "2022-08-05"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-08-05"), mapping = 
                   aes(
                     x = perc.I.2022.08.05,
                     y = Z,
                     color = "2022-08-05"
                   ),
                 size = 3
      )+
      geom_line(subset(light, Date == "2022-09-20"), mapping = 
                  aes(
                    x = perc.I.2022.09.20,
                    y = Z,
                    color = "2022-09-20"
                  ),
                size = 1
      )+
      geom_point(subset(light, Date == "2022-09-20"), mapping = 
                   aes(
                     x = perc.I.2022.09.20,
                     y = Z,
                     color = "2022-09-20"
                   ),
                 size = 3
      )+
      geom_line(subset(light, Date == "2022-11-04"), mapping = 
                 aes(
                   x = perc.I.2022.11.04,
                   y = Z,
                   color = "2022-11-04"
                 ),
               size = 1
      )+
      geom_point(subset(light, Date == "2022-11-04"), mapping = 
                 aes(
                   x = perc.I.2022.11.04,
                   y = Z,
                   color = "2022-11-04"
                 ),
               size = 3
    )+
       scale_y_reverse()+
    	xlab("Percent Irradiance")+
    	ylab("Depth (m)")+
    	scale_color_brewer(
    		"Date",
    		palette = "GnBu"
    	)+
    	theme_dark(
    		base_size = 15
    		)
    
## Save Plot
    
    ggsave("./data_analysis/plots/BC_CL_percI_Z.jpg", dpi = 400)
    
# Temperature graphs
    
    plot(chem$temp ~ chem$depth, data = chem, subset = chem$date == "2022-03-18", type = "l", ylim = c(0, 40), col = "blue")
    points(chem$temp ~ chem$depth, data = chem, subset = chem$date == "2022-05-18", type = "l", col = "purple") 
    points(chem$temp ~ chem$depth, data = chem, subset = chem$date == "2022-06-28", type = "l", col = "green") 
    points(chem$temp ~ chem$depth, data = chem, subset = chem$date == "2022-08-05", type = "l", col = "orange")
    points(chem$temp ~ chem$depth, data = chem, subset = chem$date == "2022-09-20", type = "l", col = "red") 

# Concentrated DO graphs
    
    plot(chem$conc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-03-18", type = "l", ylim = c(0,15), col = "blue")
    points(chem$conc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-05-18", type = "l", col = "purple")    
    points(chem$conc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-06-28", type = "l", col = "green")
    points(chem$conc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-08-05", type = "l", col = "orange")    
    points(chem$conc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-09-20", type = "l", col = "red")    
  
# Percent DO graphs
      
    plot(chem$perc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-03-18", type = "l", ylim = c(0, 150), col = "blue")
    points(chem$perc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-05-18", type = "l", col = "purple")    
    points(chem$perc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-06-28", type = "l", col = "green")
    points(chem$perc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-08-05", type = "l", col = "orange")    
    points(chem$perc_DO ~ chem$depth, data = chem, subset = chem$date == "2022-09-20", type = "l", col = "red")
    
# Calculate 1% light depth
    
    z.1perc.03.18 <- (light$lnI[light$Z == 0 & light$Date == "2022-03-18"] - (light$lnI[light$Z == 0 & light$Date == "2022-03-18"] * 0.01))/3.2967
    z.1perc.05.18 <- (light$lnI[light$Z == 0 & light$Date == "2022-05-18"] - (light$lnI[light$Z == 0 & light$Date == "2022-05-18"] * 0.01))/1.45526
    z.1perc.06.28 <- (light$lnI[light$Z == 0 & light$Date == "2022-06-28"] - (light$lnI[light$Z == 0 & light$Date == "2022-06-28"] * 0.01))/1.8045
    z.1perc.08.05 <- (light$lnI[light$Z == 0 & light$Date == "2022-08-05"] - (light$lnI[light$Z == 0 & light$Date == "2022-08-05"] * 0.01))/1.41544 
    z.1perc.09.20 <- (light$lnI[light$Z == 0 & light$Date == "2022-09-20"] - (light$lnI[light$Z == 0 & light$Date == "2022-09-20"] * 0.01))/2.21260
    z.1perc.11.04 <- (light$lnI[light$Z == 0 & light$Date == "2022-11-04"] - (light$lnI[light$Z == 0 & light$Date == "2022-11-04"] * 0.01))/1.6540
    
# Calculate 10% light depth
       
    z.10perc.03.18 <- (light$lnI[light$Z == 0 & light$Date == "2022-03-18"] - (light$lnI[light$Z == 0 & light$Date == "2022-03-18"] * 0.1))/3.2967
    z.10perc.05.18 <- (light$lnI[light$Z == 0 & light$Date == "2022-05-18"] - (light$lnI[light$Z == 0 & light$Date == "2022-05-18"] * 0.1))/1.45526
    z.10perc.06.28 <- (light$lnI[light$Z == 0 & light$Date == "2022-06-28"] - (light$lnI[light$Z == 0 & light$Date == "2022-06-28"] * 0.1))/1.8045
    z.10perc.08.05 <- (light$lnI[light$Z == 0 & light$Date == "2022-08-05"] - (light$lnI[light$Z == 0 & light$Date == "2022-08-05"] * 0.1))/1.41544 
    z.10perc.09.20 <- (light$lnI[light$Z == 0 & light$Date == "2022-09-20"] - (light$lnI[light$Z == 0 & light$Date == "2022-09-20"] * 0.1))/2.21260
    z.10perc.11.04 <- (light$lnI[light$Z == 0 & light$Date == "2022-11-04"] - (light$lnI[light$Z == 0 & light$Date == "2022-11-04"] * 0.1))/1.6540
    