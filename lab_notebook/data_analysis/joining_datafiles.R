# Joining Datafiles

## This file is joining datafiles into one big file

    library("tidyverse")

## Metadata

* Created: 2023-02-02 LE

## Import Data
    
    NDJ <- read.csv("./data/BC_chalgrove_lake_2022-11-04_2023-01-19.csv", header = T)
    SO <- read.csv("./data/BC_Chalgrove_lake_2022-09-20_2022-10-17.csv", header = T)
    AS <- read.csv("./data/BC_chalgrove_lake_2022-08-05_2022-09-19.csv", header = T)
    JJA <- read.csv("./data/BC_chalgrove_lake_2022-06-28_2022-08-05.csv", header = T)    
    MJ <- read.csv("./data/BC_chalgrove_lake_2022-05-20_2022-06-28.csv", header = T)    
    MAM <- read.csv("./data/BC_chalgrove_lake_2022-03-22_2022-05-18.csv", header = T)
    FM <- read.csv("./data/BC_chalgrove_lake_2022-02-18_2022-03-18.csv", header = T)
    
## Format date
    
    JJA$date.time <- as.POSIXct(JJA$date.time)
    JJA$date <- as.POSIXct(
        JJA$date,
        format = "%m/%d/%Y")
    
    MJ$date.time <- as.POSIXct(MJ$date.time)
    MJ$date <- as.POSIXct(
        MJ$date,
        format = "%m/%d/%Y")
    
    MAM$date.time <- as.POSIXct(MAM$date.time)
    MAM$date <- as.POSIXct(
        MAM$date,
        format = "%m/%d/%Y")
    
    SO$date.time <- as.POSIXct(SO$date.time)
    SO$date <- as.POSIXct(
        SO$date,
        format = "%m/%d/%Y")
    
    NDJ$date.time <- as.POSIXct(NDJ$date.time)
    NDJ$date <- as.POSIXct(
        NDJ$date,
        format = "%m/%d/%Y")
    
## Analyze Data with plots
    
    plot(DO_perc ~ Turbidity, data = AS, subset = date == "8/5/2022")
    
# Plot specific months  
    
    ggplot(JJA,
           aes(
               x = date.time,
               y = Temp
           )
    )+
        geom_line()

    
    ggplot(MJ,
           aes(
               x = date.time,
               y = Temp
           )
    )+
        geom_line()
    
    
    ggplot(MAM,
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()

    
    ggplot(SO,
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()
    
    
    ggplot(NDJ,
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()
    
# Plot a specific date    
    
    ggplot(subset(MJ, date == "2022-05-27"),
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()+
    	ylim(5, 8)
    
    
    ggplot(subset(JJA, date == "2022-07-15"),
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()
    
    
    ggplot(subset(MAM, date == "2022-04-03"),
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()
    
    
    ggplot(subset(SO, date == "2022-10-09"),
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()
    
    
    ggplot(subset(NDJ, date == "2023-01-06"),
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()+
        ylim(8, 13)
    
# Plot a specific time frame
    
    ggplot(subset(MJ, date >= "2022-06-25" & date <= "2022-06-26"),
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()
    
    
    ggplot(subset(MAM, date >= "2022-04-01" & date <= "2022-04-30"),
           aes(
               x = date.time,
               y = Temp
           )
    )+
        geom_line()+
        ylim(0, 35)
    
    
## July drop
    
    ggplot(subset(JJA, date >= "2022-07-01" & date <= "2022-07-31"),
           aes(
               x = date.time,
               y = DO_perc
           )
    )+
        geom_line()+
        ylim(0, 150)
    
    ggplot(subset(JJA, date >= "2022-07-01" & date <= "2022-07-31"),
           aes(
               x = date.time,
               y = Temp
           )
    )+
        geom_line()+
        ylim(0, 35)
    

## Weird December spikes
    
    ggplot(subset(NDJ, date >= "2022-12-01" & date <= "2022-12-31"),
           aes(
               x = date.time,
               y = DO_mgL
           )
    )+
        geom_line()+
        ylim(0, 13)
    
    ggplot(subset(NDJ, date >= "2022-12-01" & date <= "2022-12-31"),
           aes(
               x = date.time,
               y = Temp
           )
    )+
        geom_line()+
        ylim(0, 13)
    