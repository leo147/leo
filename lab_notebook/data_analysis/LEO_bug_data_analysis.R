# Script for Biodiversity Calculations with Vegan Package
## Created 2022-11-07 - KF

## Install packages

    library("vegan")
    library("tidyverse")
    
## Import Data
    
    family <- read.csv("./data/LEO_bug_data_family_2022.csv", header= T)
    genus <- read.csv("./data/LEO_bug_data_genus_2022.csv", header= T)
    
## Calculate diversity
    
    rich.fam <- specnumber(family)
    div.fam <- diversity(family)
    simp.fam <- simpson.unb(family)

    rich.gen <- specnumber(genus)
    div.gen <- diversity(genus)
    simp.gen <- simpson.unb(genus)

## Create Data Summary
  
    sites <- c("CL", "CP", "CL", "CP")
    fam.div <- data.frame(sites, rich.fam, div.fam, simp.fam)
    gen.div <- data.frame(sites, rich.gen, div.gen, simp.gen)

## Summarize Family Data
    
    fam.summary <- fam.div %>%
    	group_by(sites) %>%
    	summarise(
    		sites = sites,
    		mean.rich.fam = mean(rich.fam),
    		mean.div.fam = mean(div.fam),
    		mean.simp.fam = mean(simp.fam)
    	)
    		
    gen.summary <- gen.div %>%
    	group_by(sites) %>%
    	summarise(
    		sites = sites,
    		mean.rich.gen = mean(rich.gen),
    		mean.div.gen = mean(div.gen),
    		mean.simp.gen = mean(simp.gen)
    	)
    
## Plots
## Plot of mean +- range of richness
### Family
#### Richness
    
    ggplot(
    )+
    	geom_point(fam.summary, mapping = aes
    		(
    	x = sites,
    	y = mean.rich.fam
    			),
    		color = "blue",
    		size = 4
    		)+
    	geom_point(fam.div, mapping = aes
    		(
    	x = sites,
    	y = rich.fam
    			),
    		size = 2
    		)+
    ylim(0, 15)+
    xlab("Locations")+
    ylab("Richness")+
    theme_gray(
    	base_size = 18
    )
    
    ggsave("./data_analysis/plots/fam_rich_plot_2022.jpeg", dpi = 400)
    
#### Diversity
    
    
    ggplot(
    )+
    	geom_point(fam.summary, mapping = aes
    		(
    	x = sites,
    	y = mean.div.fam
    			),
    		color = "blue",
    		size = 4
    		)+
    	geom_point(fam.div, mapping = aes
    		(
    	x = sites,
    	y = div.fam
    			),
    		size = 2
    		)+
    ylim(0, 2)+
    xlab("Locations")+
    ylab("Diversity")+
    theme_gray(
    	base_size = 18
    )

    ggsave("./data_analysis/plots/fam_div_plot_2022.jpeg", dpi = 400)
    
#### Simpsons Diversity
    
    ggplot(
    )+
    	geom_point(fam.summary, mapping = aes
    		(
    	x = sites,
    	y = mean.simp.fam
    			),
    		color = "blue",
    		size = 4
    		)+
    	geom_point(fam.div, mapping = aes
    		(
    	x = sites,
    	y = simp.fam
    			),
    		size = 2
    		)+
    ylim(0, 1)+
    xlab("Locations")+
    ylab("Simpsons Diversity")+
    theme_gray(
    	base_size = 18
    )
    
    ggplot(fam.div, mapping = aes(
    	x = sites,
    	y = simp.fam,
    	color = sites
    )
    )+
    		stat_summary(
    			fun = mean,
    			fun.min = min,
    			fun.max = max,
    			size = 1
    		)+
    		scale_color_manual(
    			values = c(
    				"darkolivegreen2",
            "cadetblue2"
    			)
    		)+
        labs(
        	x = "Lake",
        	y = "Simpson's Diversity Index",
        	color = "Lake"
        )+
    	  ylim(
    	  	0.25, 0.75
    	  )+
    		theme_dark(
    			base_size = 20
    		)
    
    ggsave(
    	"./data_analysis/plots/CL_CP_fam_simp_plot.jpeg", 
    	dpi = 400
    	)

### Genus
#### Richness
    
    ggplot(
    )+
    	geom_point(gen.summary, mapping = aes
    		(
    	x = sites,
    	y = mean.rich.gen
    			),
    		color = "brown",
    		size = 4
    		)+
    	geom_point(gen.div, mapping = aes
    		(
    	x = sites,
    	y = rich.gen
    			),
    		size = 2
    		)+
    ylim(0, 15)+
    xlab("Locations")+
    ylab("Richness")+
    theme_gray(
    	base_size = 18
    )
    
    ggsave("./data_analysis/plots/gen_rich_plot_2022.jpeg", dpi = 400)

#### Diversity
    
    ggplot(
    )+
    	geom_point(gen.summary, mapping = aes
    		(
    	x = sites,
    	y = mean.div.gen
    			),
    		color = "brown",
    		size = 4
    		)+
    	geom_point(gen.div, mapping = aes
    		(
    	x = sites,
    	y = div.gen
    			),
    		size = 2
    		)+
    ylim(0, 2)+
    xlab("Locations")+
    ylab("Diversity")
    
    ggsave("./data_analysis/plots/gen_div_plot_2022.jpeg", dpi = 400)

#### Simpsons Diversity
    
    ggplot(
    )+
    	geom_point(gen.summary, mapping = aes
    		(
    	x = sites,
    	y = mean.simp.gen
    			),
    		color = "brown",
    		size = 5
    		)+
    	geom_point(gen.div, mapping = aes
    		(
    	x = sites,
    	y = simp.gen
    			),
    		size = 3
    		)+
    ylim(0, 1)+
    xlab("Locations")+
    ylab("Simpsons Diversity")+
    theme_gray(
    	base_size = 18
    )
    
    ggsave("./data_analysis/plots/gen_simp_plot_2022.jpeg", dpi = 400)
   
## Statistics
    
    anova(lm(
    	simp.fam ~ sites,
    	data = fam.div
    ))

     ##################################################    
     # ANOVA Results of family Simpsons Diversity by Site
    
     Analysis of Variance Table

     Response: simp.fam
               Df   Sum Sq Mean Sq F value   Pr(>F)   
     sites      1 0.174643 0.17464  198.53 0.004999 **
     Residuals  2 0.001759 0.00088 

     ##################################################     


    anova(lm(
    	simp.gen ~ sites,
    	data = gen.div
    ))

     ##################################################    
     # ANOVA Results of genus Simpsons Diversity by Site
    
     Analysis of Variance Table

     Response: simp.gen
              Df   Sum Sq  Mean Sq F value   Pr(>F)   
     sites      1 0.178335 0.178335  267.02 0.003724 **
     Residuals  2 0.001336 0.000668     

     ##################################################     