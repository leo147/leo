# Analysis of the pond environmental data
## Description

This code is for the analysis of the environmental data in Chalgrove Lake and Campus Pond.

## Metadata: 

* created 2023-03-03 - KF: modified from code created 2023-02-02 by Helena Loucas

## INSTALL PACKAGES

       library("vegan")
       library("tidyverse")

## Import Data
### CHALGROVE LAKE DATA

      CL.02.18thru03.18 <- read.csv("./data/BC_chalgrove_lake_2022-02-18_2022-03-18.csv", header=T)

      CL.03.22thru05.18 <- read.csv("./data/BC_chalgrove_lake_2022-03-22_2022-05-18.csv", header=T)
      
      CL.05.20thru06.28 <- read.csv("./data/BC_chalgrove_lake_2022-05-20_2022-06-28.csv", header=T)
      
      CL.06.28thru08.05 <- read.csv("./data/BC_chalgrove_lake_2022-06-28_2022-08-05.csv", header=T)
      
      CL.08.05thru09.19 <- read.csv("./data/BC_chalgrove_lake_2022-08-05_2022-09-19.csv", header=T)
      
      CL.09.20thru10.17 <- read.csv("./data/BC_chalgrove_lake_2022-09-20_2022-10-17.csv",header=T)
      
      CL.11.04thru01.19 <- read.csv("./data/BC_chalgrove_lake_2022-11-04_2023-01-19.csv", header=T)
   
### CAMPUS POND DATA 
      
      CP.02.17thru03.14 <- read.csv("./data/BC_CampusPond_2022-02-17_2022-03-14.csv", header=T)
      
      CP.03.18thru05.11 <- read.csv("./data/BC_CampusPond_2022-03-18_2022-05-11.csv", header=T)
      
      CP.05.11thru06.13 <- read.csv("./data/BC_CampusPond_2022-05-11_2022-06-13.csv", header=T)
      
      CP.06.13thru07.08 <- read.csv("./data/BC_CampusPond_2022-06-13_2022-07-08.csv", header=T)
      
      CP.07.08thru09.01 <- read.csv("./data/BC_CampusPond_2022-07-08_2022-09-01.csv", header=T)
      
      CP.10.10thru12.21 <- read.csv("./data/BC_CampusPond_2022-10-10_2022-12-21.csv", header=T)
      
#### Correct the DO and Temp data from when the DO sensor cap expired
      
      CP.10.10thru12.21$DO[CP.10.10thru12.21$DO <= -800] <- NA
      CP.10.10thru12.21$Temp.DO[CP.10.10thru12.21$Temp.DO <= -800] <- NA
      
      CP.12.21thru02.24 <- read.csv("./data/BC_CampusPond_2022-12-21_2023-02-24.csv", header=T)

      
### JOINING CHALGROVE LAKE DATA
      
      CLA <- full_join (
         CL.02.18thru03.18,
         CL.03.22thru05.18
      )
      CLB <- full_join (
         CLA,
         CL.05.20thru06.28
      )
      
      CLC <- full_join (
         CLB,
         CL.06.28thru08.05
      )
      
      CLD <- full_join (
         CLC,
         CL.08.05thru09.19
      )
     
      CL <- full_join (
         CLD,
         CL.09.20thru10.17
      )
      
      # Due to bad pH measurements, this data set is not being used
      #CL <- full_join (
         #CLE,
         #CL.11.04thru01.19
      #)
      
### JOINING CAMPUS POND DATA
      
      CPA <- full_join (
         CP.02.17thru03.14,
         CP.03.18thru05.11
         )
      CPB <- full_join (
         CPA,
         CP.05.11thru06.13
         )     
      CPC <- full_join (
         CPB,
         CP.06.13thru07.08
      )     
      CPD <- full_join (
         CPC,
         CP.07.08thru09.01
      )           
      CPE <- full_join (
         CPD,
         CP.10.10thru12.21
      )
      CP <- full_join (
         CPE,
         CP.12.21thru02.24
      )

## Convert date.time to POSIX
      
      CL$date.time <- as.POSIXct(CL$date.time)
      CP$date.time <- as.POSIXct(CP$date.time)

## Make a month.year variable
      
### Chalgrove Lake
      
      month <- format(CL$date.time, "%B")
      year <- format(CL$date.time, "%Y")
      month.year <- format(CL$date.time, "%Y-%B")
      month.year <- factor(
      	month.year,
      	levels = c(
      		"2022-February",
      	  "2022-March",
      	  "2022-April",
      	  "2022-May",
      	  "2022-June",
      	  "2022-July",
      	  "2022-August",
      	  "2022-September",
      	  "2022-October"
      	)
      )
      date.time <- CL[, 1]
      CL.month.year <- data.frame(
      	date.time,
      	month.year,
      	month,
      	year
      )
      
### Campus Pond
      
      month <- format(CP$date.time, "%B")
      year <- format(CP$date.time, "%Y")
      month.year <- format(CP$date.time, "%Y-%B")
      month.year <- factor(
      	month.year,
      	levels = c(
      		"2022-February",
      	  "2022-March",
      	  "2022-April",
      	  "2022-May",
      	  "2022-June",
      	  "2022-July",
      	  "2022-August",
      	  "2022-September",
      	  "2022-October",
      		"2022-November",
      		"2022-December",
      		"2023-January",
      		"2023-February",
      		NA
      	)
      )
      date.time <- CP[, 1]
      CP.month.year <- data.frame(
      	date.time,
      	month.year,
      	month,
      	year
      )

# Ordination 
# Chalgrove Lake
## Create appropriate matrix
### Remove unneeded columns from data.frame
#### Retain only 6 (Cond), 10 (DO_mgL), 14 (Turb), 16 (pH), 18 (Temp)
      
      CL.trunc <- CL[, c(6, 10, 14, 16, 18)]
      CL.month.year.trunc <- data.frame(
      	CL.month.year,
      	CL.trunc
      )
      
#### Remove all rows that contain NAs
      
      CL.comp <- CL.month.year.trunc[
      	complete.cases(
      		CL.month.year.trunc
      		), 
      	]
      
## Check Variables
### Chalgrove Lake
      
      ggplot(CL.comp, mapping = aes(
      	x = date.time,
      	y = Cond
      )
      )+
      	geom_point()
      		
      ggplot(CL.comp, mapping = aes(
      	x = date.time,
      	y = DO_mgL
      )
      )+
      	geom_point()
      
      ggplot(CL.comp, mapping = aes(
      	x = date.time,
      	y = Turbidity
      )
      )+
      	geom_point()
      
      ggplot(CL.comp, mapping = aes(
      	x = date.time,
      	y = pH
      )
      )+
      	geom_point()
      
      ggplot(CL.comp, mapping = aes(
      	x = date.time,
      	y = Temp
      )
      )+
      	geom_point()
      
### Convert data.frame into a matrix
      
      CL.mat <- as.matrix(
      	CL.comp[, c(5, 6, 7, 8, 9)]
      	) # This selects only the rows with numeric data 
      
## Scale measurements to same scale
      
      CL.scale <- scale(CL.mat)
      
## PCA Analysis
### NOTE: The PCA can be done on the scaled matrix (CP.scale) or the data.frame (CP.comp)
      
      CL.PCA <- prcomp(CL.scale)

### Summary of PCA

      summary(CL.PCA)
      
      ################################################## 
      Importance of components:
                                PC1    PC2    PC3    PC4     PC5
      Standard deviation     1.4924 1.1361 0.9538 0.7112 0.25806
      Proportion of Variance 0.4454 0.2581 0.1820 0.1012 0.01332
      Cumulative Proportion  0.4454 0.7036 0.8855 0.9867 1.00000
      ##################################################

      CL.PCA
      
      ##################################################
      Standard deviations (1, .., p=5):
      [1] 1.4923647 1.1360965 0.9538052 0.7111908 0.2580614

      Rotation (n x k) = (5 x 5):
                        PC1        PC2         PC3         PC4         PC5
      Cond       0.55209250 -0.2767108  0.26260705  0.52915724  0.51928330
      DO_mgL    -0.46606648 -0.4634678 -0.12685883  0.65352702 -0.35325472
      Turbidity  0.27191275  0.1855699 -0.92122213  0.19067560  0.08136263
      pH        -0.02753302 -0.7928733 -0.24984858 -0.49953085  0.24215400
      Temp       0.63504433 -0.2134116  0.06220814 -0.08370664 -0.73504933
      ##################################################

### Add PC Scores to the data.frame
      

      	CL.comp.PCA <- data.frame(
      		CL.comp,
      		CL.PCA$x
      	)

### Plot of PC1 and PC2 in a scatter plot
      
      ggplot(CL.comp.PCA, mapping = aes(
      	x = PC1,
      	y = PC2,
      	color = month.year
      )
      )+
      	geom_point(
      		pch = 19)+
      	scale_color_manual(
      		values = colorRampPalette(
      			c("blue", "green", "orange", "blue")
      			)(13)
      		)+
      	labs(
      		color = "Year-Month"
      	)+
      	theme_dark(
      		base_size = 15
      		)
      
      ggsave(
      	"./data_analysis/plots/CL_PCA_plot.jpeg", 
      	dpi = 400
      )
      		
### Plot of PC1 by date 
      
      ggplot(CL.comp.PCA, mapping = aes(
      	x = month.year,
      	y = PC1,
      	color = month.year
      )
      )+
      	geom_violin(
      		)+
      	scale_color_manual(
      		values = colorRampPalette(
      			c("blue", "green", "orange", "blue")
      			)(13)
      		)+
      	labs(
      		color = "Year-Month"
      	)+
      	theme_dark(
      		base_size = 15
      		)
      
      ggsave(
      	"./data_analysis/plots/CL_PC1_by_date_plot.jpeg", 
      	dpi = 400
      )
      
### Plot of PC2 by date 
      
      ggplot(CL.comp.PCA, mapping = aes(
      	x = month.year,
      	y = PC2,
      	color = month.year
      )
      )+
      	geom_violin(
      		)+
      	scale_color_manual(
      		values = colorRampPalette(
      			c("blue", "green", "orange", "blue")
      			)(13)
      		)+
      	labs(
      		color = "Year-Month"
      	)+
      	theme_dark(
      		base_size = 15
      		)
      
      ggsave(
      	"./data_analysis/plots/CL_PC2_by_date_plot.jpeg", 
      	dpi = 400
      )
      
# Campus Pond
## Create appropriate matrix
### Remove unneeded columns from data.frame
#### Retain only DO (4), Full_Range_CT (9), WaterTemp (14), Z (21)
      
      CP.trunc <- CP[, c(4, 9, 14, 21)]
      CP.month.year.trunc <- data.frame(
      	CP.month.year,
      	CP.trunc
      )
      
#### Remove all rows that contain NAs
      
      CP.comp <- CP.month.year.trunc[
      	complete.cases(
      		CP.month.year.trunc
      		), 
      	]
      
## Check Variables
### Campus Pond Lake
      
      ggplot(CP.comp, mapping = aes(
      	x = date.time,
      	y = DO
      )
      )+
      	geom_point()
      
      ggplot(CP.comp, mapping = aes(
      	x = date.time,
      	y = Full_Range_CT
      )
      )+
      	geom_point()
      
#### Remove rows that contain CT > 500 - these are likely non-representative
      
      CP.comp <- CP.comp[
        CP.comp$Full_Range_CT <= 500, 
      ]
      
      # re-plot CT
      ggplot(CP.comp, mapping = aes(
        x = date.time,
        y = Full_Range_CT
      )
      )+
        geom_point()
      
      ggplot(CP.comp, mapping = aes(
      	x = date.time,
      	y = WaterTemp
      )
      )+
      	geom_point()
      
      ggplot(CP.comp, mapping = aes(
      	x = date.time,
      	y = Z
      )
      )+
      	geom_point()
      
### Convert data.frame into a matrix
      
      CP.mat <- as.matrix(
      	CP.comp[, c(5, 6, 7, 8)]
      	) # This selects only the rows with numeric data DP (5), Full_Range_CT (6), WaterTemp (7), and Z (8)
      
## Scale measurements to same scale
      
      CP.scale <- scale(CP.mat)
      
## PCA Analysis
### NOTE: The PCA can be done on the scaled matrix (CP.scale) or the data.frame (CP.comp)
      
      CP.PCA <- prcomp(CP.scale)

### Summary of PCA

      summary(CP.PCA)
      
      ################################################## 
      Importance of components:
                                PC1    PC2    PC3     PC4
      Standard deviation     1.3855 1.0251 0.8049 0.61786
      Proportion of Variance 0.4799 0.2627 0.1620 0.09544
      Cumulative Proportion  0.4799 0.7426 0.9046 1.00000
      ##################################################

      CP.PCA
      
      ##################################################
      Standard deviations (1, .., p=4):
        [1] 1.3855025 1.0250805 0.8048828 0.6178644
      
      Rotation (n x k) = (4 x 4):
                           PC1        PC2        PC3        PC4
      DO            -0.5682118  0.2242756 -0.5753200  0.5439143
      Full_Range_CT  0.5291211  0.2517835 -0.7389104 -0.3326369
      WaterTemp      0.5598295  0.4241522  0.2444634  0.6685234
      Z              0.2894083 -0.8404763 -0.2515073  0.3828661
      ##################################################

### Add PC Scores to the data.frame
      

      	CP.comp.PCA <- data.frame(
      		CP.comp,
      		CP.PCA$x
      	)

### Plot of PCA
      
      ggplot(CP.comp.PCA, mapping = aes(
      	x = PC1,
      	y = PC2,
      	color = month.year
      )
      )+
      	geom_point(
      		pch = 19)+
      	scale_color_manual(
      		values = colorRampPalette(
      			c("blue", "green", "orange", "blue")
      			)(13)
      		#palette = "Set2"
      	)+
      	labs(
      		color = "Year-Month"
      	)+
      	theme_dark(
      		base_size = 15
      		)
     
      ggsave(
      	"./data_analysis/plots/CP_PCA_plot.jpeg",
      	dpi = 400
      )
      
# Compare CL to CP
## Combine the data shared between the ponds

      # NOTE: I am beginning with the truncated complete data.frames from above
      
### Remove un-shared variables 
#### For CL.comp retain 1 (CL...1.), 2 (month.year), 3 (month), 4 (year), 5 (Cond), 6 (DO_mgL), 9 (Temp)
      
      CL.comp.shared <- CL.comp[, c(1, 2, 3, 4, 5, 6, 9)]
      
#### For CP.comp retain 1 (CP...1.), 2 (month.year), 3 (month), 4 (year), 5 (DO), 6 (Full_Range_CT), 7 (WaterTemp)
      
      CP.comp.shared <- CP.comp[, c(1:7)]

### Rename variables in data.frames to match
      
      names(CL.comp.shared) <- c(
      	"date.time", 
      	"month.year",
      	"month",
      	"year",
      	"CT",
      	"DO",
      	"Temp"
      )

      names(CP.comp.shared) <- c(
      	"date.time",
      	"month.year",
      	"month",
      	"year",
      	"DO",
      	"CT",
      	"Temp"
      )

## Join the CP and CL data.frames
### Create "lake" variable
      
      lake <- c(
      	rep(
      		"CL",
      		times = length(CL.comp.shared$date.time)
      	),
      	rep(
      		"CP",
      		times = length(CP.comp.shared$date.time)
      	)
      )
      
      lake <- factor(
        lake, 
        levels = c(
          "CL",
          "CP"
        )
      )

### Join the data.frames
      
      CL.CP.comp.shared <- 
      	full_join(
      		CL.comp.shared,
      		CP.comp.shared
      	)

### Combine the joind data.frames with the lake name variable
      
      CL.CP.comp.shared <- data.frame(
      	lake,
      	CL.CP.comp.shared
      )

#### Test plots
      
      ggplot(CL.CP.comp.shared, mapping = aes(
      	x = date.time,
      	y = Temp,
      	color = lake
      )
      )+
      	geom_point(
      		pch = 21
      		)
      
      ggplot(CL.CP.comp.shared, mapping = aes(
      	x = date.time,
      	y = DO,
      	color = lake
      )
      )+
      	geom_point(
      		pch = 19 
      		)+
      	labs(
      		x = " ",
      		y = "Dissolved Oxygen Concentration (mg/L)",
      		color = "Lake"
      	)+
      	scale_color_manual(
      		values = c(
            "darkolivegreen3",
            "cadetblue3"
          )
        )+
      	theme_dark(
      		base_size = 15
      	)
      
      ggsave(
      	"./data_analysis/plots/CL_CP_DO_by_date.jpeg",
      	dpi = 400
      )
 
      ggplot(CL.CP.comp.shared, mapping = aes(
      	x = date.time,
      	y = CT,
      	color = lake
      )
      )+
      	geom_point(
      		pch =19 
      		)
      
## Run PCA on combined data from both lakes
### Convert data.frame into a matrix
      
      CL.CP.mat <- as.matrix(
      	CL.CP.comp.shared[, c(6, 7, 8)]
      	) # This selects only the rows with numeric data CT (6), DO (7), and Temp (8)
      
## Scale measurements to same scale
      
      CL.CP.scale <- scale(CL.CP.mat)
      
## PCA Analysis
### NOTE: The PCA can be done on the scaled matrix (CP.scale) or the data.frame (CP.comp)
      
      CL.CP.PCA <- prcomp(CL.CP.scale)

### Summary of PCA

      summary(CL.CP.PCA)
      
      ################################################## 
      Importance of components:
                                PC1    PC2    PC3
      Standard deviation     1.2605 0.9416 0.7243
      Proportion of Variance 0.5296 0.2955 0.1749
      Cumulative Proportion  0.5296 0.8251 1.0000
      ##################################################

      CL.CP.PCA
      
      ##################################################
      Standard deviations (1, .., p=3):
        [1] 1.2604539 0.9415937 0.7243322
      
      Rotation (n x k) = (3 x 3):
                  PC1        PC2         PC3
      CT    0.6557424 -0.2216861  0.72170439
      DO   -0.4071258 -0.9088533  0.09074283
      Temp  0.6358070 -0.3533284 -0.68622774
      ##################################################

### Add PC Scores to the data.frame

      	CL.CP.comp.PCA <- data.frame(
      		CL.CP.comp.shared,
      		CL.CP.PCA$x
      	)

### Plot of PCA
      
      ggplot(CL.CP.comp.PCA, mapping = aes(
      	x = PC1,
      	y = PC2,
      	color = lake
      )
      )+
      	geom_point(
      		pch = 19 
      		)+
        scale_color_manual(
          values = c(
            "darkolivegreen3",
            "cadetblue3"
          )
        )+
      	theme_dark(
      		base_size = 20
      	)
      
      ggsave(
      	"./data_analysis/plots/CL_CP_PCA_plot.jpeg",
      	dpi = 400
      )
      
      ggplot(CL.CP.comp.PCA, mapping = aes(
      	x = PC1,
      	y = lake,
      	fill = lake
      )
      )+
      	geom_violin(
      	  )+
        stat_summary(
          fun = mean,
        	color = "black",
          size = 1
        )+
        stat_summary(
          fun = median,
          color = "red",
          size = 1
        )+
        scale_fill_manual(
          values = c(
            "darkolivegreen3",
            "cadetblue3"
          )
        )+
      	labs(
      		x = "PC1",
      		y = "Lake"
      	)+
      	theme_dark(
      		base_size = 20
      		)+
        theme(
          legend.position="none"
        )
      
      ggsave(
      	"./data_analysis/plots/CL_CP_PC1_violin_plot.jpeg",
      	dpi = 400
      )
      
      ggplot(CL.CP.comp.PCA, mapping = aes(
      	x = lake,
      	y = PC2,
      	fill = lake
      )
      )+
      	geom_violin(
      	  )+
        stat_summary(
          fun = mean,
        	color = "black",
          size = 1
        )+
        stat_summary(
          fun = median,
          color = "red",
          size = 1
        )+
        scale_fill_manual(
          values = c(
            "darkolivegreen3",
            "cadetblue3"
          )
        )+
      	labs(
      		x = "Lake",
      	)+
      	theme_dark(
      		base_size = 20
      		)+
        theme(
          legend.position="none"
        )
      
      ggsave(
      	"./data_analysis/plots/CL_CP_PC2_violin_plot.jpeg",
      	dpi = 400
      )
      